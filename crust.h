/*
	This header file must be included in all source files with CRUST tags
	It defines by default all the crust-specific tags as empty macros,
	ensuring that they are transparent to the C compiler.
	These tags will be kept only if ENABLE_CRUST_TAGS is defined, which
	is something that only crust itself does (or should do).
*/

#ifndef ENABLE_CRUST_TAGS

#ifndef __crust__
#define __crust__
#endif

#ifndef __crust_borrow__
#define __crust_borrow__
#endif

#ifndef __crust_recycle__
#define __crust_recycle__
#endif

#ifndef __crust_alias__
#define __crust_alias__
#endif

#ifndef __crust_no_0__
#define __crust_no_0__
#endif

#ifndef __crust_not_null__
#define __crust_not_null__
#endif

#ifndef __crust_enable__
#define __crust_enable__
#endif

#ifndef __crust_disable__
#define __crust_disable__
#endif

#ifndef __crust_full_enable__
#define __crust_full_enable__
#endif

#ifndef __crust_debug__
#define __crust_debug__
#endif

#ifndef __crust_set_null__
#define __crust_set_null__(p)
#endif

#ifndef __crust_set_not_null__
#define __crust_set_not_null__(p)
#endif

#ifndef __crust_override__
#define __crust_override__
#endif

#ifndef __crust_no_warning__
#define __crust_no_warning__
#endif

#endif
