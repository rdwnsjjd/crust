#!/usr/bin/env python3

# Copyright 2017 (C) Raster Software Vigo (Sergio Costas)
#
# This file is part of CRUST
#
# CRUST is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License.
#
# CRUST is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

from ctypes import *
from crust import tokens
import sys
import subprocess
import gettext

import glob
import os
import pkg_resources
import copy
from crust.crust_open import crust_open

_ = gettext.gettext


class crust_helpers(crust_open):

    def __init__(self, headerpath, libpath, do_print_debug = False):
        super().__init__(headerpath, libpath)
        self.print_debug = do_print_debug
        self.debug = []
        self.errors = []
        self.disabled_warnings = {}

    def get_errors_for_test(self):
        output = []
        for error in self.errors:
            if ((error[0] == self.MSG_WARNING) and
                (error[2] in self.disabled_warnings) and
                (error[3] in self.disabled_warnings[error[2]])):
                    continue
            output.append([error[0], error[1]] + list(error[4:]))
        return output


    def disable_warning_at(self, file, line):
        if file not in self.disabled_warnings:
            self.disabled_warnings[file] = {}
        self.disabled_warnings[file][line] = True

    def _do_debug(self, thread_status):
        if self.print_debug:
            print("Debug:")
            for block in thread_status["variables"]:
                print("  Block:")
                for variable in block:
                    print("    Variable: {:s}".format(str(variable)))
                    print("      status: {:s}".format(self.state_str[(block[variable]["value"])]))
                    print("      pointer: {:d}".format(block[variable]["pointer"]))
                    print("      uid: {:s}".format(str(block[variable]["uid"])))
            print("")
        self.debug.append(self._copy_status(thread_status))


    def set_debug_lines(self, start_line, end_line):
        self.libcrust.set_debug_values(start_line, end_line)


    def _copy_status(self, status):
        """ Fully copies a thread_status to allow several parallel emulations """

        retval = { "variables": [] }
        for element in status:
            if element == "variables":
                continue
            retval[element] = status[element]
        for element in status["variables"]:
            vargroup = {}
            for varname in element:
                vargroup[varname] = element[varname].copy()
            retval["variables"].append(vargroup)
        return retval


    def print_errors(self, warnings, allwarnings, delete_errors = True):
        nerrors = 0
        hidden_errors = 0
        for err in self.errors:
            if (err[0] == self.MSG_CRITICAL):
                nerrors += 1
                print("\033[1;35m{:s}\033[0m: ".format(_("CRITICAL")), end = "")
            elif (err[0] == self.MSG_ERROR):
                nerrors += 1
                print("\033[1;31m{:s}\033[0m: ".format(_("ERROR")), end = "")
            elif (err[0] == self.MSG_WARNING):
                if not warnings:
                    hidden_errors += 1
                    continue
                if (err[2] in self.disabled_warnings) and (err[3] in self.disabled_warnings[err[2]]) and (not allwarnings):
                    hidden_errors += 1
                    continue
                nerrors += 1
                print("\033[1;33m{:s}\033[0m: ".format(_("WARNING")), end = "")
            print(_(err[1]).format(*tuple(err[4:])))
        print("Total: {:d} errors ({:d} hidden).".format(nerrors, hidden_errors))
        if delete_errors:
            self.errors = []


    def _add_error(self, thread_status, err_type, err_line, err_file, err_value, *argv):
        if thread_status["debug_level"] == 0:
            group = (err_type, err_value, err_file, err_line) + argv
            if self.errors.count( group ) == 0:
                self.errors.append( group )
            # this line allows to detect formatting errors in the error messages in the line
            # where they have been produced, and not at the end, when they will be printed
            msg = err_value.format(*argv)



    def _find_variable(self, thread_status, var_name, line_number, filename, searching_function = False, set_used = True):
        for var_group in thread_status["variables"]:
            if var_name in var_group:
                if set_used:
                    var_group[var_name]["accessed"] = True
                return var_group[var_name]
        if not searching_function:
            self._add_error(thread_status, self.MSG_CRITICAL, line_number, filename, "Unknown variable '{:s}' at line {:d}", str(var_name), line_number)
        raise self.VarNotFoundException()


    def _find_function(self, thread_status, function_name, line_number, filename):
        try:
            function_name = self._find_variable(thread_status, function_name, line_number, "find_function", True)
            return function_name["function_params"]
        except:
            pass
        self._add_error(thread_status, self.MSG_CRITICAL, line_number, filename, "Calling function '{:s}' at line {:d}, but it is neither declared nor defined", str(function_name), line_number)
        raise self.FunctionNotFoundException()


    def _free_block(self, thread_status, var_name, line_number, filename):
        variable = self._find_variable(thread_status, var_name, line_number, "freeblock")
        if variable["value"] == self.VALUE_GLOBAL:
            return

        if variable["borrowed"]:
            return

        if variable["function"]:
            self._add_error(thread_status, self.MSG_CRITICAL, line_number, filename, "Trying to free a function name '{:s}' at line {:d}", str(var_name), line_number)
            return

        block_id = variable["uid"]
        # mark as free all variables with that block id
        # this will mark as freed the global variables with that block and the aliases
        if block_id is not None:
            for blocks in thread_status["variables"]:
                for varname in blocks:
                    if blocks[varname]["uid"] == block_id:
                        blocks[varname]["init_line"] = line_number
                        if blocks[varname]["value"] == self.VALUE_NOT_NULL:
                            blocks[varname]["value"] = self.VALUE_FREED
                        elif blocks[varname]["value"] == self.VALUE_NOT_NULL_OR_NULL:
                            blocks[varname]["value"] = self.VALUE_FREED_OR_NULL
                        blocks[varname]["uid"] = None
        else:
            variable["init_line"] = line_number
            if variable["value"] == self.VALUE_NOT_NULL:
                variable["value"] = self.VALUE_FREED
            elif variable["value"] == self.VALUE_NOT_NULL_OR_NULL:
                variable["value"] = self.VALUE_FREED_OR_NULL
            variable["uid"] = None


    def _set_var_value(self, thread_status, var_name, value, line_number, filename, force = False):
        """ Set the new state of a variable """

        variable = self._find_variable(thread_status, var_name, line_number, "set_var_value")
        if variable["value"] == self.VALUE_GLOBAL:
            return
        if variable["function"]:
            if variable["pointer"] == 0:
                self._add_error(thread_status, self.MSG_CRITICAL, line_number, filename, "Trying to assign a value to the function name '{:s}' at line {:d}", str(var_name), line_number)
            return
        if (not force) and variable["crust"]:
            state = variable["value"]
            if ((state == self.VALUE_NOT_NULL) or (state == self.VALUE_NOT_NULL_OR_NULL)) and (not variable["borrowed"]) and (not variable["alias"]):
                # If this block is already assigned to another variable, it is OK
                # (due to the checks in CRUST, it can be assigned to several global variables, but only to a single main variable)
                found = False
                for blocks in thread_status["variables"]:
                    for varname in blocks:
                        var2 = blocks[varname]
                        if var2 == variable:
                            continue
                        if var2["alias"]: # don't check aliases
                            continue
                        if (var2["uid"] == variable["uid"]) and (var2["uid"] is not None):
                            found = True
                            break
                    if found:
                        break
                if not found:
                    if (variable["init_line"] == -1) or (variable["init_line"] is None):
                        self._add_error(thread_status, self.MSG_ERROR, line_number, filename, "Assignment to '{:s}' at line {:d}, but it is not freed", var_name, line_number)
                    else:
                        self._add_error(thread_status, self.MSG_ERROR, line_number, filename, "Assignment to '{:s}' at line {:d}, which was already assigned at line {:d}", var_name, line_number, variable["init_line"])
            variable["init_line"] = line_number
        block_id = variable["uid"]
        if force and (block_id is not None):
            for blocks in thread_status["variables"]:
                for varname in blocks:
                    if blocks[varname]["uid"] == block_id:
                        blocks[varname]["value"] = value
        else:
            variable["value"] = value


    def _status_to_condition(self, retval):
        if retval == self.VALUE_NULL:
            return self.CONDITION_FALSE
        elif retval == self.VALUE_NOT_NULL:
            return self.CONDITION_TRUE
        else:
            return self.CONDITION_FALSE_TRUE


    def _check_can_be_used(self, thread_status, variable, line_number, filename, comparison):
        if (variable.type != "IDENTIFIER"):
            return
        varname = self._find_variable(thread_status, variable.name, line_number, "check_can_be_used")
        if not varname["crust"]:
            return
        if varname["value"] == self.VALUE_UNINITIALIZED:
            self._add_error(thread_status, self.MSG_ERROR, line_number, filename, "Using uninitialized variable '{:s}' at line {:d}", variable.name, line_number)
            return
        if (comparison == False) and ((varname["value"] == self.VALUE_FREED) or (varname["value"] == self.VALUE_FREED_OR_NULL)):
            self._add_error(thread_status, self.MSG_ERROR, line_number, filename, "Using variable '{:s}' at line {:d}, after being freed at line {:d}", variable.name, line_number, varname["init_line"])
            return



    def _check_statement_is_crust(self, statement, vardata = None):
        """ Returns whether the statement is a true crust type (to avoid managing cast-typed elements)
        @params:
            statement -- the node with the element to evaluate
            vars -- the variable's dictionary to check, or None if no dictionary is available, or only the statement data must be used
        """

        if statement is None:
            return False

        if vardata is None:
            if statement.t_crust and (statement.pointer == 1) and (statement.right is None):
                return True
            else:
                return False
        else:
            if vardata["crust"] and (statement.right is None) and (not vardata["array"]):
                return True
            else:
                return False


    def _compare_types(self, var1, var2, deep_check = False):
        if var1["crust"] != var2["crust"]:
            return False
        if var1["borrowed"] != var2["borrowed"]:
            return False
        if var1["recycle"] != var2["recycle"]:
            return False
        if var1["pointer"] != var2["pointer"]:
            return False
        if deep_check:
            if var1["not_null"] != var2["not_null"]:
                return False
        return True


    def _get_next_block(self, current_block):

        """ Given a piece of the tree, cuts it in two subtrees: one with the block
            at the beginning (if it is not a { } block, will be only the first statement)
            and another with the remaining """

        if current_block[0].type != "START_BLOCK":
            # The block has only one statement, so it isn't a "block"
            return [current_block[0]], current_block[1:]
        depth = 0
        pos = 0
        while True:
            if current_block[pos].type == "START_BLOCK":
                depth += 1
            elif current_block[pos].type == "END_BLOCK":
                depth -= 1
            pos += 1
            if depth == 0:
                break
        # return the block with both START_BLOCK and END_BLOCK statements
        return current_block[0:pos], current_block[pos:]


    def _assign_block_start(self, blocks):
        start_blocks = []
        counter = 0
        for block in blocks:
            block.index_value = counter
            counter += 1
            if block.type == "START_BLOCK":
                start_blocks.append(block)
            elif block.type == "END_BLOCK":
                block.block_start = start_blocks[-1]
                start_blocks = start_blocks[:-1]


    def _get_loops(self, switch_block):
        loops = {}
        while len(switch_block) > 0:
            if ((switch_block[0].type == "WHILE") or
                (switch_block[0].type == "DO") or
                (switch_block[0].type == "FOR")):
                    block1, block2 = self._get_next_block(switch_block[1:])
                    inner_loops = self._get_loops(block1)
                    loops.update(inner_loops)
                    first_block = copy.deepcopy(switch_block[0])
                    if first_block.type == "FOR":
                        first_block.for_ch1 = None
                    loops[block1[-1]] = [first_block] + block1
                    switch_block = block2
            else:
                switch_block = switch_block[1:]
        return loops

    def _get_cases_in_switch(self, switch_block):
        self._assign_block_start(switch_block)
        loops = self._get_loops(switch_block)
        default_block = None
        default_element = None
        blocks = {}
        while len(switch_block) > 0:
            if switch_block[0].type == "CASE":
                blocks[switch_block[0]] = switch_block[1:]
            elif switch_block[0].type == "DEFAULT":
                default_element = switch_block[0]
                default_block = switch_block[1:]
            switch_block = switch_block[1:]
        new_blocks = {}
        for case_block in blocks:
            block = blocks[case_block]
            new_blocks[case_block] = self._repeat_blocks(case_block, block, loops)
        blocks = new_blocks
        if default_block:
            default_block = self._repeat_blocks(default_element, default_block, loops)
        return blocks, default_block


    def _repeat_blocks(self, case_element, block, loops):
        newblock = []
        while len(block) > 0:
            element = block[0]
            if ((element.type == "WHILE") or
                (element.type == "DO") or
                (element.type == "FOR")):
                    block1, block2 = self._get_next_block(block[1:])
                    newblock += [element] + block1
                    block = block2
                    continue
            elif element.type == "END_BLOCK":
                if element in loops:
                    newblock += loops[element]
                else:
                    if element.block_start.index_value > case_element.index_value:
                        newblock.append(element)
                block = block[1:]
            else:
                newblock.append(element)
                block = block[1:]
        return newblock

    def _get_variable_properties(self, thread_status, node, pointers, is_parameter, retval_from_function = None):
        """ Process a node that represents a variable or function definition, and returns a dictionary with its properties.
            Properties:
                name: the variable or function name
                crust: if TRUE, it is a crust_type variable
                borrowed: if TRUE, it is a borrowed, crust_type function argument
                recycle: if TRUE, it is a recycle, crust_type function argument
                not_null: if TRUE, it is an argument that can't be NULL
                is_param: if TRUE, it is a function argument
                void: if is a void type (useful for return values in functions)
                status: the current status of the block pointed by this variable, in case it is a crust one. It can be:
                    - VALUE_UNINITIALIZED: it hasn't been initialized yet to point to a block
                    - VALUE_NULL: it has been initialized and its value is NULL; thus, it doesn't point to a block
                    - VALUE_NOT_NULL: it has been initialized and its value is not NULL; thus, it does point to a block
                    - VALUE_NOT_NULL_OR_NULL: it has been initialized, but it is unknown if the value is NULL (due to a function returning an error, or not being able to initialize a memory block) or not NULL (this is, really pointing to a valid block)
                    - VALUE_FREED: it pointed to a valid block previously, but that block has been passed into a function, so currently it must be considered a dangling pointer
                    - VALUE_GLOBAL: it is a global variable, so it will be considered always as VALUE_NOT_NULL_OR_NULL
                    - VALUE_FUNCTION: it is not a variable, but a function. This means that this variable can't be set.
                pointer: the number of indirections that this variable represents. Thus, "int a;" will have 0; "int *a;" will have 1; "int **a;" will have 2; "int a[];" will have 1...
                struct: TRUE if this variable is an struct
                array: TRUE if this variable is an array
                function: TRUE if this is a function, or a pointer to a function. In the first case, status will be VALUE_FUNCTION
                function_params: a list with the return values and the arguments of the function or function pointer, or None if this is not a function or function pointer
                ellipsis: if it is a function definition, this field will be TRUE if the argument list end with ellipsis (...) to specify a function with a variable number of arguments
                init_line: the line in which this variable was first initialized
                global: whether this variable is or not a global variable
                uid: the Unique ID of the block stored in this variable, or None if there is no block
                alias: the variable is an alias type
                accessed: TRUE if the variable has been accessed (read or write)
        """

        retval = {"name": None, "crust": False, "borrowed": False, "recycle": False, "not_null": False, "is_parameter": is_parameter, "void": False, "enum": False, "value": self.VALUE_UNINITIALIZED, "pointer": pointers, "struct": False, "override": False, "array": False, "function": False, "function_params": None, "ellipsis": False, "init_line": -1, "global": False, "uid": None, "alias": False }
        if not isinstance(node, self.AST_node):
            return retval

        retval["name"] = node.name

        if node.t_crust:
            if node.pointer == 0:
                if is_parameter:
                    if node.name is None:
                        self._add_error(thread_status, self.MSG_CRITICAL, node.line, node.filename, "An argument is defined as __crust__ type at line {:d}, but it is not a pointer", node.line)
                    else:
                        self._add_error(thread_status, self.MSG_CRITICAL, node.line, node.filename, "Argument '{:s}' defined as __crust__ type at line {:d}, but it is not a pointer", node.name, node.line)
                else:
                    if retval_from_function is not None:
                        self._add_error(thread_status, self.MSG_CRITICAL, node.line, node.filename, "Return value for function '{:s}', defined at line {:d}, is defined as __crust__ type, but it is not a pointer", retval_from_function, node.line)
                    else:
                        self._add_error(thread_status, self.MSG_CRITICAL, node.line, node.filename, "Variable '{:s}' is defined as __crust__ type at line {:d}, but it is not a pointer", node.name, node.line)
            elif node.pointer == 1:
                retval["crust"] = True
            else:
                retval["crust"] = False

        if node.t_crust_recycle:
            if not is_parameter:
                self._add_error(thread_status, self.MSG_CRITICAL, node.line, node.filename, "Variable '{:s}', defined at line {:d}, have the __crust_recycle__ modifier, which is not allowed for defined variables", node.name, node.line)

        if (node.t_crust_borrow and node.t_crust_recycle):
            if is_parameter:
                if node.name is None:
                    self._add_error(thread_status, self.MSG_CRITICAL, node.line, node.filename, "An argument can be BORROW or RECYCLE, not both. (error at line {:d})", node.line)
                else:
                    self._add_error(thread_status, self.MSG_CRITICAL, node.line, node.filename, "An argument can be BORROW or RECYCLE, not both. (argument '{:s}' at line {:d})", node.name, node.line)

        if node.t_override:
            retval["override"] = True

        if node.t_crust_borrow:
            retval["borrowed"] = True

        if node.t_crust_recycle:
            retval["recycle"] = True

        if node.t_crust_not_null:
            retval["not_null"] = True

        if (node.arrays is not None):
            retval["array"] = True

        if node.t_struct:
            retval["struct"] = True

        if node.t_void:
            retval["void"] = True

        if node.t_ellipsis:
            retval["ellipsis"] = True

        if node.t_crust_alias:
            retval["alias"] = True

        retval["accessed"] = False
        retval = self._node_fill_parameters(thread_status, node, retval)

        return retval


    def _node_fill_parameters(self, thread_status, node, var_data):

        if node.function_params is not None:
            parameters = [ var_data.copy() ]
            var_data["crust"] = False
            for parameter in node.function_params:
                if parameter.t_void and (parameter.pointer == 0) and not parameter.function:
                    continue
                parameters.append( self._get_variable_properties(thread_status, parameter, parameter.pointer, True) )
            var_data["function_params"] = parameters
        return var_data


    def _process_enum(self, enum_node, thread_status):
        counter = 0
        for node in enum_node.enum_data:
            tmpdata = self._get_variable_properties(thread_status, None, 0, False)
            tmpdata["value"] = self.VALUE_GLOBAL
            tmpdata["init_line"] = node.line
            tmpdata["enum"] = True
            thread_status["variables"][0][node.name] = tmpdata


    def _create_param(self, thread_status, name, param_type):
        pointer = 0
        while (param_type[-1] == '*'):
            param_type = param_type[:-1]
            pointer += 1
        param = self._get_variable_properties(thread_status, name, pointer, False)
        return param


    def _add_builtin_function(self, thread_status, function_name, retval_type, parameters):
        tmpparams = self._get_variable_properties(thread_status, None, 0, False)
        tmpparams["function"] = True
        tmpparams["value"] = self.VALUE_FUNCTION
        tmpparams["function_params"] = [ self._create_param(thread_status, function_name, retval_type) ]
        for param in parameters:
            tmpparams["function_params"].append(self._create_param(thread_status, "", param))
        tmpparams["name"] = function_name
        tmpparams["ellipsis"] = False
        tmpparams["definition_line"] = -1
        tmpparams["definition_file"] = "gcc_builtin"
        thread_status["variables"][0][function_name] = tmpparams


    def _check_return_block(self, tree, thread_status):
        if len(tree) == 0:
            return [ (None, thread_status) ]
        else:
            return [ (tree, thread_status) ]


    def _print_tree(self, tree):
        for node in tree:
            print("{:s} at line {:d}".format(node.type, node.line))
        print("\n\n")


    def _check_global_vars(self, thread_status, line, filename):
        # check that there are no global vars with the same block,
        # neither global vars in FREED state
        copy_var = {}
        namesvar = []
        for variable in thread_status["variables"][-1]:
            data = thread_status["variables"][-1][variable]
            if not data["crust"]:
                continue
            if not data["global"]:
                continue
            # we create two copies to be able to check if a block is pointed by more than one global variable
            copy_var[variable] = data.copy()
            namesvar.append(variable)
            if (data["value"] == self.VALUE_FREED) or (data["value"] == self.VALUE_FREED_OR_NULL):
                self._add_error(thread_status, self.MSG_ERROR, line, filename, "At exit point in line {:d}, global variable '{:s}' points to a block freed at line {:d}.", line, variable, data["init_line"])

        counter = 0
        namesvar.sort() # sorting to ensure that the message order is consistent, thus avoiding mistakes in the TESTS
        for var1 in namesvar[:-1]:
            counter += 1
            for var2 in namesvar[counter:]:
                if var1 == var2:
                    continue
                if copy_var[var1]["uid"] == copy_var[var2]["uid"]:
                    self._add_error(thread_status, self.MSG_ERROR, line, filename, "At exit point in line {:d}, global variable '{:s}' points to the same block than global variable '{:s}'.", line, var1, var2)


    def _check_blocks_in_use(self, thread_status, vars, line, filename):
        # check if there are memory blocks in use
        for variable in vars:
            data = vars[variable]
            if data["global"]:
                if (data["value"] == self.VALUE_FREED) or (data["value"] == self.VALUE_FREED_OR_NULL):
                    self._add_error(thread_status, self.MSG_ERROR, line, filename, "At exit point in line {:d}, static variable '{:s}' points to a block freed at line {:d}.", line, variable, data["init_line"])
                continue
            if not data["crust"]:
                continue
            if ((data["value"] == self.VALUE_NOT_NULL) or (data["value"] == self.VALUE_NOT_NULL_OR_NULL)) and (not data["borrowed"]) and (not data["alias"]):
                found = False
                # check if this block is already stored in a global variable
                blocks = thread_status["variables"][-1]
                for varname in blocks:
                    if not blocks[varname]["global"]:
                        continue
                    if blocks[varname]["uid"] == data["uid"]:
                        found = True
                        break
                if not found:
                    init_line = data["init_line"]
                    if init_line is None:
                        self._add_error(thread_status, self.MSG_ERROR, line, filename, "Memory block '{:s}' is still in use at exit point in line {:d}", variable, line)
                    else:
                        self._add_error(thread_status, self.MSG_ERROR, line, filename, "Memory block '{:s}', initialized at line {:d}, is still in use at exit point in line {:d}", variable, init_line, line)


    def _compare_status(self, ex_point, status, status_list):
        """ Gets the current status and compares it with all the previous status in the same execution point.
            If the current status has been already found, it returns TRUE. If not, it will be stored and will return FALSE

            Parameters:
                ex_point: the execution point UID
                status: the status to compare
                status_list: a dictionary with arrays with all the old statuses to check
            Returns:
                TRUE if the status has been already checked; FALSE if not
        """

        # First, copy all the variables, removing every not pointer
        current_vars = []
        for element in status["variables"]:
            vargroup = {}
            for varname in element:
                if (element[varname]["crust"]) or (element[varname]["pointer"] > 0):
                    # only copy pointers and CRUST type variables
                    vargroup[varname] = element[varname].copy()
            current_vars.append(vargroup)

        if not ex_point in status_list:
            # if there are no status stored for this point, create a new group
            status_list[ex_point] = [ current_vars ]
            return False

        for old_expoint in status_list[ex_point]:
            if old_expoint == current_vars:
                # if there is an status identical to the current, return True
                return True

        # there is no status identical to the current, so add the current to the list
        status_list[ex_point].append(current_vars)
        return False

    def _new_start_block(self):
        node = self.AST_node()
        node.type = "START_BLOCK"
        return node

    def _new_end_block(self, replace_block):
        node = self.AST_node()
        node.type = "END_BLOCK"
        node.line = replace_block.line
        node.filename = replace_block.filename
        node.position = replace_block.position
        return node

    def _new_end_loop(self):
        node = self.AST_node()
        node.type = "END_LOOP"
        return node

    def _new_tmp_end_loop(self):
        node = self.AST_node()
        node.type = "TMP_END_LOOP"
        return node


    def _new_eval_false(self, condition):
        node = self.AST_node()
        node.type = "EVAL_FALSE"
        node.condition = condition.copy()
        return node

    def _new_eval_true(self, condition):
        node = self.AST_node()
        node.type = "EVAL_TRUE"
        node.condition = condition.copy()
        return node
