#!/usr/bin/env python3

# Copyright 2017 (C) Raster Software Vigo (Sergio Costas)
#
# This file is part of CRUST
#
# CRUST is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License.
#
# CRUST is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

import sys
import glob
import os
import pkg_resources
from crust.crust_class import crust

# this code is the entry point

def process_file(lib, filename, parameters):
    print("Analizing file {:s}".format(filename))
    if (parameters["debug1"] is not None) and (parameters["debug2"] is not None):
        lib.set_debug_lines(parameters["debug1"], parameters["debug2"])
    try:
        tree = lib.read_file(filename, False, parameters["includes"], parameters["defines"], parameters["include_files"], parameters["nocpp"], parameters["preprocessed"])
        if tree is None:
            return
        variables = lib.process_tree(tree, filename, parameters["verbose"])
        lib.process_functions(tree, variables)
    except crust.VarNotFoundException:
        pass
    except crust.FunctionNotFoundException:
        pass
    lib.print_errors(parameters["warnings"], parameters["allwarnings"])


def get_parameters():

    parameters = { "includes":[], "include_files":[], "files":{}, "defines":[], "allwarnings":False, "warnings":True, "verbose":True, "nocpp":False, "debug1":None, "debug2":None, "preprocessed":None }
    exclude_files = []
    store_next = None
    for parameter in sys.argv[1:]:
        if parameter.strip() == "":
            continue
        if store_next is not None:
            if store_next == exclude_files:
                exclude_files.append(parameter)
            elif isinstance(parameters[store_next], list):
                parameters[store_next].append(parameter)
            else:
                parameters[store_next] = parameter
            store_next = None
            continue
        if parameter[0] != '-':
            files = glob.glob(parameter)
            for f in files:
                parameters["files"][f] = True
            continue
        if parameter == "--include":
            store_next = "include_files"
            continue
        if parameter.startswith("-I"):
            if parameter == "-I":
                store_next = "includes"
            else:
                parameters["includes"].append(parameter[2:])
            continue
        if parameter.startswith("-D"):
            if parameter == "-D":
                store_next = "defines"
            else:
                parameters["defines"].append(parameter[2:])
            continue
        if (parameter == "-v") or (parameter == "--version"):
            print("Crust version: {:s}".format(crust.version))
            sys.exit(0)
        if (parameter == "-h") or (parameter == "--help"):
            print("CRUST {:s}".format(crust.version))
            print("Usage:")
            print("    crust [-I include_path] [-I...] [-D define] [-D...] [--include filename] [--include...] [--nowarnings] [--nocpp] [--quiet] [-e exclude_filename] [--debug1=nline] [--debug2=nline] file.c [file.c] ...")
            print()
            print("        -I             allows to specify paths where the C preprocessor will search for header files. Can be repeated as many times as paths have to be added")
            print("        -D             allows to specify #defines from command line that the C preprocessor must consider defined. Can be repeated as many times as defines are needed")
            print("        --include       allows to include a file in the final preprocessed code. Can be repeated as many times as defines are needed")
            print("        --nowarnings   will hide the WARNING messages, showing only ERROR and CRITICAL messages")
            print("        --allwarnings  will show all the WARNING messages, even those manually hidden")
            print("        --nocpp        will pass the source code directly to the parser, instead of pass it before through the C preprocessor")
            print("        --quiet        will not show extra information")
            print("        -e             allows to specify file names to exclude. This is useful when a filename contains wildcards")
            print("        --preprocessed stores the preprocessed code (this is, after being passed through the C PreProcessor) with the specified filename")
            print("        --debug1 & --debug2 shows the parsed tree between source code lines specified in both statements")
            print("        file.c        the path to the file or files to analyze. It can contain wildcards, and it is possible to also specify several files")
            print()
            print()
            print("    crust --headers")
            print()
            print("        Creates the file crust.h in the current folder, needed to compile files that includes crust specific tags")
            print()
            print()
            sys.exit(0)
        if parameter == "--preprocessed":
            store_next = "preprocessed"
        if parameter == "--nocpp":
            parameters["nocpp"] = True
            continue
        if parameter == "--allwarnings":
            parameters["allwarnings"] = True
            continue
        if parameter == "--nowarnings":
            parameters["warnings"] = False
            continue
        if parameter == "--quiet":
            parameters["verbose"] = False
            continue
        if parameter.startswith("-e"):
            if parameter == "-e":
                store_next = exclude_files
            else:
                exclude_files.append(parameter[2:])
            continue
        if parameter == "--headers":
            header = pkg_resources.resource_filename('crust','/crust.h')
            with open(header, "r") as header2:
                with open("crust.h", "w") as header3:
                    for linea in header2:
                        header3.write(linea)
            sys.exit(0)
        if parameter.startswith("--debug1="):
            try:
                parameters["debug1"] = int(parameter[9:])
            except:
                pass
            continue
        if parameter.startswith("--debug2="):
            try:
                parameters["debug2"] = int(parameter[9:])
            except:
                pass
            continue
    for f in parameters["files"]:
        f2 = os.path.basename(f)
        if 0 != exclude_files.count(f2):
            parameters["files"][f] = False
    return parameters

def main():
    if (os.path.exists('lexyacclib/crust_code.h')) and (os.path.exists('lexyacclib/crust.so')):
        lib = crust("lexyacclib/crust_code.h", "lexyacclib/crust.so", True)
    else:
        lib = crust(pkg_resources.resource_filename('crust','/crust_code.h'),pkg_resources.resource_filename('crust','/crust.so'), True)
    parameters = get_parameters()
    filelist = []
    for f in parameters["files"]:
        filelist.append(f)
    if len(filelist) == 0:
        print("No files")
    else:
        filelist.sort()
        for f in filelist:
            if parameters["files"][f]:
        	       process_file(lib, f, parameters)

if __name__ == "__main__":
    main()
