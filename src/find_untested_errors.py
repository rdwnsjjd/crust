#!/usr/bin/env python3

import sys

messages = {}

with open("crust/crust.py", "r") as origin:
    n = 0
    for line in origin.readlines():
        n += 1
        line = line.strip()
        if line.startswith("self._add_error"):
            status = 0 # find the open parenteses
            msg = ""
            for c in line:
                if status == 0:
                    if c == '(':
                        status = 1 # first parameter
                    continue
                if status == 1:
                    if c == ',':
                        status = 2 # second parameter
                    continue
                if status == 2:
                    if c == ',':
                        status = 3 # third parameter
                    continue
                if status == 3:
                    if c == '"':
                        status = 4 # start of the third parameter
                    continue
                if status == 4:
                    if c == '"': # end of the third parameter
                        break
                    msg += c
            messages[msg] = n

with open("error_list.txt", "r") as testlist:
    for line in testlist.readlines():
        line = line.strip()
        if line in messages:
            messages[line] = True
    for msg in messages:
        if messages[msg] == True:
            continue
        print("{:d}: {:s}".format(messages[msg],msg))
