typedef __crust__ unsigned char *crust_t;

crust_t function(void);
//crust_t p;
void main(void) {

    static crust_t p;
    __crust_set_not_null__(p);
    p = function();
}
