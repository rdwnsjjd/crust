typedef __crust__ unsigned char *crust_t;

void function(crust_t);

void main(void) {
	function(2); // ERROR: calling a function passing a constant (non-crust) value as a crust parameter
}
