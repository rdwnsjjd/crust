typedef __crust__ unsigned char *crust_t;

#define NULL ((void *)0)

void main(crust_t param) {

	crust_t var2;
	var2 = param;
	return; // ERROR: var2 hasn't been managed
}
