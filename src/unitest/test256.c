void function(__crust_not_null__ void (*cb)(void)) {
    cb(); // must produce an error (cb can be NULL)
}

void function3(void) {
    function(NULL);
}
