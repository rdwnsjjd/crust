typedef __crust__ unsigned char *crust_t;

#define NULL ((void *)0)

void function1(crust_t __crust_not_null__ param);

void main(crust_t param) {

	function1(param); // ERROR: the parameter must be not_null, but it is not possible to guarantee that
}
