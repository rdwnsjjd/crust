#define NULL ((void *)0)

void free(void *);

struct crust_ts {
    char var1;
    int var2;
    struct crust_ts *next;
};

typedef __crust__ struct crust_ts* crust_t;

void function(crust_t var1, char *var2) {

    free(var1); // free() is a valid way of disposing a crust variable
    free(var2);
}
