typedef __crust__ unsigned char *crust_t;

int function(crust_t);

void main(void) {

	int *param = 1;
	function(param); // ERROR: calling a function passing a non-crust value as a crust parameter
}
