typedef __crust__ unsigned char crust_t; // define a NON pointer typedef, but the arguments or variables are of pointer type

void function(crust_t *param3) { // here: a pointer

	int retval = function(param3);
	int retval2 = function(param3); // ERROR: param3 was freed at line 5, when passed to "function"
}
