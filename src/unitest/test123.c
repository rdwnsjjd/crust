typedef __crust__ unsigned char *crust_t;

#define NULL ((void *)0)

void main(crust_t param) {

	int b;

	for(b = 0; b == 0; b++) {
		main(param); // ERROR: when executed two times, param is used after being freed
	}
}
