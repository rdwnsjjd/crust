typedef __crust__ unsigned char *crust_t;
crust_t test[5];

void function(crust_t);
void main(void) {
	crust_t param4 = test; // ERROR: it is an array, not a crust_t element
	function(param4);
}
