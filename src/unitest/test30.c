typedef __crust__ unsigned char *crust_t;

void function(crust_t param3) {

	function(param3);
	function(param3); // ERROR: calling a function with a freed parameter
}
