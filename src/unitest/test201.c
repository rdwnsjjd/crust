typedef __crust__ unsigned char *crust_t;

crust_t var1;

void function(crust_t);

void main(crust_t __crust_not_null__ arg1) {

    __crust_debug__
    __crust_set_null__(var1);
    __crust_debug__

    var1 = arg1; // fine, and will not fail because the status of var1 is considered NULL
    arg1 = NULL; // It is fine because it has been assigned to a global variable
}
