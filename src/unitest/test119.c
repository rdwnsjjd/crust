typedef __crust__ unsigned char *crust_t;

#define NULL ((void *)0)

crust_t global1;
void function2(crust_t param);

void function(crust_t param) {

	if (global1 == NULL) {
		global1 = param; // It is fine: since we are storing the block from "param3" in a global variable, it is not a dangling pointer
	} else {
		function2(param);
	}
}
