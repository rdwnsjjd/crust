typedef __crust__ unsigned char *crust_t;

#define NULL ((void *)0)

crust_t function1(void);

void main(void) {

	char *var1;

	var1 = (char *) function1(); // This is valid because of the cast

}
