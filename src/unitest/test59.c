void printf(char *);

struct test {
	int b;
} a;

void main(void) {

	if (a->b >= 1) {
		printf("1\n");
	} else {
		if (a->b >= 2) {
			printf("2\n");
		} else {
			printf("no 1,2\n"); // all is legal
		}
	}
}
