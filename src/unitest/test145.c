typedef __crust__ unsigned char *crust_t;

typedef crust_t (*prueba)();

typedef __crust__ struct {
	prueba function_p;
} *tmp;

#define NULL ((void *)0)

crust_t function(void);

void function2(crust_t);

void main(tmp param1) {
	param1->function_p = function; // WARNING: using param1 when it is possible to it to be NULL
	param1->function_p(); // WARNING: using param1 when it is possible to it to be NULL
	function2(param1);
}
