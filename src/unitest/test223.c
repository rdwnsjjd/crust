typedef __crust__ unsigned char *crust_t;

__crust_override__ void test_function(crust_t __crust_borrow__ param);
void test_function(unsigned char *param); // this one must be ignored

#define NULL ((void *)0)

void main(crust_t __crust_borrow__ param) {
	test_function(param);
}
