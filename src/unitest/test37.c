typedef __crust__ unsigned char *crust_t;

struct {
	crust_t value;
} test;

void function2(crust_t);

void function(void) {
	crust_t param4 = test; // ERROR: assigning a non-crust variable into a crust variable
	function2(param4);
}
