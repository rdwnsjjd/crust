#define NULL ((void *)0)

void *realloc(void *ptr, int size);

struct crust_ts {
    char var1;
    int var2;
    struct crust_ts *next;
};

typedef __crust__ struct crust_ts* crust_t;

crust_t function(crust_t var1) {

    return (crust_t) realloc(var1, 800);
}
