typedef __crust__ unsigned char *crust_t;

void function(crust_t param);

void main(crust_t param, int a) {

	switch (a) {
		case 0:
			function(param);
			break;
		case 1:
			function(param);
			break;
		case 2:
			function(param);
			break;
	}
    // ERROR: if 'a' is 3 or more, param isn't freed
}
