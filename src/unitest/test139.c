typedef __crust__ unsigned char *crust_t;

#define NULL ((void *)0)

void function(crust_t param);

void main(crust_t param1, crust_t param2, crust_t param3) {

	for __crust_no_0__ (int b=0; b < 4; b++) {

		for __crust_no_0__ (int c = 0; c < 4; b++) {
			break;
			for __crust_no_0__ (int d = 0; d < 4; d++) {
				function(param1); // this never should be called due to the break at line 12
			}
			function(param2); // this also should never be called
		}
		function(param3); // this must be called always
		break;
	}
	// here, param3 must be freed, but not param1, neither param2
}
