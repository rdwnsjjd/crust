typedef __crust__ unsigned char *crust_t;

#define NULL ((void *)0)

void function(crust_t param);

void main(crust_t param1, crust_t param2) {

	for __crust_no_0__ (int b=0; b < 4; b++) {
		function(param2); // this must be called
		break;
		function(param1); // this must not be called
	}
	// here, param2 must be freed, but not param1
}
