typedef __crust__ unsigned char *crust_t;

void function(crust_t);

void main(void) {

	function(3,5); // ERROR: incorrect number of parameters
}
