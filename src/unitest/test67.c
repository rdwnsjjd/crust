typedef __crust__ unsigned char *crust_t;

#define NULL ((void *)0)

void main(crust_t param) {

	crust_t var2;

	var2 = param;

	if (var2 == NULL) {
		return;
	}
	main(var2); // 'var2' has been managed when it is not NULL. All this is legal
}
