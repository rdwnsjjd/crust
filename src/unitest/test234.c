typedef __crust__ unsigned char *crust_t;

void function(crust_t);

void main(crust_t p) {

    if (!p) {
        int a = 5;
    } else {
        function(p);
    }
}
