typedef __crust__ unsigned char *crust_t;

typedef char* (*prueba)(crust_t);

#define NULL ((void *)0)

char* function(crust_t);

void main(void) {
	prueba test1 = function;
	(*test1)(NULL,NULL); // ERROR: calling a function pointer with an incorrect number of parameters
}
