#define NULL ((void *)0)

struct crust_ts {
    char var1;
    int var2;
    struct crust_ts *next;
};

typedef __crust__ struct crust_ts* crust_t;

crust_t get_crust_var(void);
void consume_crust_var(crust_t);

int main(crust_t var) {

    int a = 5;

    while(1) {
        consume_crust_var(var);
        if (a == 7) {
            return 0;
        }
        var = get_crust_var();
    }
    return 0;
}
