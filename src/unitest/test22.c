typedef __crust__ int * crust_t;

void function(crust_t param1) {
	crust_t param4 = param1;
	crust_t param5 = param1; // ERROR: assigning a previously freed variable (due to assign in line 4)
	// ERROR: param4 isn't freed
}
