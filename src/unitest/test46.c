typedef __crust__ unsigned char *crust_t;

#define NULL ((void *)0)

void main(crust_t param) {

	crust_t var2 = (crust_t) 5;
	if (param != NULL) {
		main(param);
	} else {
		param = var2;
		main(param); // all is legal
	}
}
