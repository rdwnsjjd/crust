typedef __crust__ unsigned char *crust_t;

#define NULL ((void *)0)

void main(__crust_borrow__ crust_t param1, crust_t param2) {
	__crust_borrow__ crust_t var1;
	__crust_borrow__ crust_t var2;
	__crust_borrow__ crust_t var3;
	__crust_borrow__ crust_t var4;

	var1 = param1; // valid
	var2 = param2; // ERROR: can't assign a non-borrow variable to a borrowed one
	var3 = NULL; // valid
	var4 = (crust_t) 5; // ERROR: can't assign a free value to a borrowed variable
}
