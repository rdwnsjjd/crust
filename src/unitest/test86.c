typedef __crust__ unsigned char *crust_t;

#define NULL ((void *)0)

char* function(crust_t);

void main(void) {
	char* (*test1)(crust_t) = function;
	(*test1)(NULL,NULL); // ERROR: calling a function pointer with incorrect number of parameters
}
