typedef __crust__ int *crust_t;

#define NULL ((void *)0)

void main(crust_t param) {

	crust_t __crust_alias__ an_alias = param;
	main(an_alias); // this must work, and no error has to be emited because 'param' has not been freed, because it is aliased by 'an_alias'
	__crust_debug__
}
