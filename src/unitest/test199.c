#define NULL ((void *)0)

struct crust_ts {
    char var1;
    int var2;
    struct crust_ts *next;
};

typedef __crust__ struct crust_ts* crust_t;

crust_t function2(void);

void function(void) {

    crust_t __crust_alias__ tmp1;
    tmp1 = function2; // ERROR: can't assign a function pointer to an alias
}
