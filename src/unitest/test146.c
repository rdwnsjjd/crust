typedef __crust__ unsigned char *crust_t;

typedef crust_t (*prueba)();

typedef __crust__ struct {
	prueba function_p;
} *crust2_t;

#define NULL ((void *)0)

crust_t function(void);

void main(crust2_t param1) {
	main(param1);
	param1->function_p = function; // ERROR: param1 has been freed
}
