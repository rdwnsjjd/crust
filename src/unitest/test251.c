// not allowed: C doesn't check that calls to functions without parameters honors the signature
// Thus, it is possible to call this function with several parameters, or assign it to a pointer
// to function with any type and number of parameters.

// To avoid this, it is a must to add "void" inside the parameter list;
// this is, the right definition is:
// int function(void) {}
// and also in the declaration in test251.h

#include "test251.h"

void function() {
}