typedef __crust__ unsigned char *crust_t;

#define NULL ((void *)0)

void function2(crust_t);

void function(int v, crust_t data) {

	switch (v) {
		case '0' ... '7':
			return;
	}
	function2(data);
}
