typedef __crust__ unsigned char *crust_t;

#define NULL ((void *)0)

void process_block(crust_t __crust_borrow__ block);

void main(crust_t param) {

	// This loop works fine
	for(__crust_alias__ crust_t looper = param; looper != NULL; looper = looper->next) {
		process_block(looper);
	}
}
