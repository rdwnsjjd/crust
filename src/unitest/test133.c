__crust__ struct t_crust_t {
	int value;
	__crust__ struct t_crust_t *next;
}

typedef struct t_crust_t  *crust_t;

#define NULL ((void *)0)

void tmpfunc(void);

void main(crust_t param) {

	goto END_TEST;

	main(param); // this must not be called, because of the GOTO statement

END_TEST:

	tmpfunc();
}
