typedef __crust__ unsigned char *crust_t;

crust_t main(crust_t param) {
	crust_t param2;
	param2 = ++param; // it is illegal to use preincrement, predecrement, postincrement or postdecrement with crust pointers
	param2--;

	int a = 5;
	a++;
	return param2;
}
