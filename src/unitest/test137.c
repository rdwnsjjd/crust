typedef __crust__ unsigned char *crust_t;

#define NULL ((void *)0)

crust_t main(int param) {

	return param; // Error: expects a crust value, but a non-crust value is returned
}
