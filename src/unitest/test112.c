typedef __crust__ unsigned char *crust_t;

#define NULL ((void *)0)

crust_t function(void);

void main(void) {

	crust_t param;
	int b;

	param = (function(), b=6); // ERROR: function returns a crust-type value, but it is discarded
}
