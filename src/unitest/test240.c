typedef __crust__ unsigned char *crust_t;

void function(crust_t);

void main(crust_t __crust_not_null__ p) {

    // Here, p in in NOT_NULL state
    function(p);
    // Now, p is in FREED state

    if (!p) { // NO ERROR, because we are just comparing
        p = NULL;
    }
}
