typedef __crust__ unsigned char *crust_t;

#define NULL ((void *)0)

void function(crust_t param);

void main(crust_t param1, crust_t param2, crust_t param3, int a) {



	switch (a) {
		case 0:
			function(param1);
			break;
		case 1:
			function(param2);
			break;
		case 3:
			function(param3);
			break;
		default:
			function(param1);
			function(param2);
			function(param3);
		break;
	} // ERROR: if 'a' is 0, only 'param1' is freed; if 'a' is 1 only 'param2' is freed; if 'a' is 3 only 'param3' is freed
}
