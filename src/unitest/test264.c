typedef __crust__ struct {
	int element;
} *crust_t;

#define NULL ((void *)0)

void main(crust_t __crust_borrow__ param) {

	int var2 = param->element;
	// All is legal, except if we assume that 'param' can have a NULL value. That's why it returns a WARNING
	param->element = var2; __crust_no_warning__
}
