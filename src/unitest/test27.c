typedef __crust__ unsigned char *crust_t;

void function(void) {

	crust_t param = 5; // ERROR: assigning a constant (non-crust) to a crust variable
	//ERROR: param is in use at the end of the function
}
