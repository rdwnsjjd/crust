typedef __crust__ unsigned char *crust_t;

struct {
    char var1;
    int var2;
} global_var;

void function(crust_t);

void main(crust_t __crust_not_null__ var) {

    global_var.var1 = var; // this is fine
    __crust_debug__
}
