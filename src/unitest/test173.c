typedef __crust__ unsigned char *crust_t;

#define NULL ((void *)0)

void function1(crust_t param);

void main(crust_t __crust_not_null__ param1, crust_t param2) {

	if (param1 == NULL) {
		function1(param2); // This will never be called because param1 is considered to never be NULL
	}
	function1(param1);
	function1(param2); // This is valid because the line 10 will never be executed, since param1 is __crust_not_null__
}
