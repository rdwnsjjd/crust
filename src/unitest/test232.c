typedef __crust__ unsigned char *crust_t;

crust_t var1;

void main(int p) {

    if (p == 0) {
        __crust_set_null__(var1); // this is allowed because we haven't accessed var1
    } else {
        __crust_set_not_null__(var1); // just ensuring that one execution path doesn't affect others
    }
}
