typedef __crust__ unsigned char *crust_t;

crust_t var1;

void function(crust_t);

void main(crust_t __crust_not_null__ arg1) {

    if (var1 == NULL) {
        var1 = arg1;
        function(var1);
    } else {
        function(arg1);
    }
    // ERROR: there is a global variable with FREED status at exit
}
