typedef __crust__ unsigned char *crust_t;

#define NULL ((void *)0)

void function(crust_t param);

void main(crust_t param1, int a) {

	switch (a) {
		case 0:
			function(param1);
		case 1:
			function(param1); // ERROR: when 'a' is 0, 'param1' will be freed at line 11, so when the execution reachs line 13 (because there is no 'break') 'param1' will be freed
		case 3:
			function(param1); // ERROR: the same when 'a' is 0 or 1
			break;
		default:
			function(param1);
		break;
	}
}
