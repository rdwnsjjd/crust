typedef __crust__ unsigned char *crust_t;

#define NULL ((void *)0)

crust_t __crust_borrow__ function1(void);

void main(void) {

	function1(); // It is legal to not use a returned crust value if it is borrowed
}
