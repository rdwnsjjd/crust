typedef __crust__ unsigned char *crust_t;

#define NULL ((void *)0)

void function(crust_t __crust_borrow__);

void function2(crust_t);

void main(void) {

	crust_t var2 = (crust_t) 5;
	function(var2); // This is legal
	__crust_debug__
	function2(var2);
}
