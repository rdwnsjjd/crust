typedef __crust__ unsigned char *crust_t;

#define NULL ((void *)0)

crust_t __crust_borrow__ function1(void);

void main(__crust_borrow__ crust_t param1) {

	param1 = function1(); // ERROR: assigning to a borrowed parameter is not allowed
}
