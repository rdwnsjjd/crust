#define NULL ((void *)0)

void *realloc(void *ptr, int size);

struct crust_ts {
    char var1;
    int var2;
    struct crust_ts *next;
};

typedef __crust__ struct crust_ts* crust_t;

void function(crust_t var1) {

    crust_t var2 = NULL;

    var2 = (crust_t) realloc(var1, 800);
}
