typedef __crust__ unsigned char *crust_t;

#define NULL ((void *)0)

void function(crust_t param);

void main(crust_t param1, int a) {

	switch (a) {
		case 0:
			function(param1);
			break;
		case 1:
			function(param1);
			break;
		case 3:
			function(param1);
			break;
		default:
			function(param1);
		break;
	}
	function(param1); // ERROR: 'param1' has been freed in all previous cases
}
