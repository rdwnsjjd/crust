typedef __crust__ unsigned char *crust_t;

void function(crust_t);

void main(void) {

	function(2); // ERROR: passed a constant (non-crust) as a crust parameter
}
