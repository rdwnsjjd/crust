typedef __crust__ unsigned char *crust_t;

void main(void) {

    crust_t var1;

    __crust_set_null__(var1); // ERROR: it is not a global variable

}
