typedef __crust__ unsigned char *crust_t;

#define NULL ((void *)0)

crust_t __crust_not_null__ main(crust_t data) {

	// WARNING: the return value must be not NULL, but it can be NULL. But the warning is masked.
	return data; __crust_no_warning__;
}
