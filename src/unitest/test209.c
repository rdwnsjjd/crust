typedef __crust__ unsigned char *crust_t;

#define NULL ((void *)0)


void function(crust_t __crust_not_null__);
void function2(crust_t);

void function(crust_t parameter) { // ERROR: the types doesn't match
    function2(parameter);
}
