typedef __crust__ unsigned char *crust_t;

void function(crust_t);

void main(void) {

    static crust_t p;
    __crust_set_not_null__(p);
    function(p);
}
