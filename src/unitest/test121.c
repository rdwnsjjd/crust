typedef __crust__ unsigned char *crust_t;

#define NULL ((void *)0)

void main(crust_t param) {

	do {
		if (param == NULL) {
			param = (crust_t) 1;
		}
	} while(param == NULL);
	main(param); // No error
}
