typedef __crust__ unsigned char *crust_t;

void function1(crust_t param);

crust_t function2(crust_t param) {
	function1(param);
	return param; // ERROR: returning a freed parameter
}
