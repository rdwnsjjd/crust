typedef __crust__ unsigned char *crust_t;

crust_t var1;

void function(crust_t);

void main(crust_t __crust_not_null__ arg1) {

    if (var1 == NULL) {
        var1 = arg1; // fine
        __crust_debug__
        arg1 = NULL; // It is fine because it has been assigned to a global variable
        __crust_debug__
    } else {
        function(arg1);
    }
}
