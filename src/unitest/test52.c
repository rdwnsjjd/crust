typedef __crust__ unsigned char *crust_t;

#define NULL ((void *)0)

void test_function(crust_t __crust_borrow__ param) {
}

void main(crust_t __crust_borrow__ param) {
	void (*prueba)(crust_t __crust_borrow__);
	prueba = test_function; // this is legal
}
