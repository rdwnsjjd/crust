typedef __crust__ unsigned char *crust_t;

typedef crust_t (*prueba)();

#define NULL ((void *)0)

crust_t function(int);

void main(void) {
	prueba test1 = function; // ERROR: the number of parameters is different, and there are crust-type parameters involved (the return values)
}
