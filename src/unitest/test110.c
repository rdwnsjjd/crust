typedef __crust__ unsigned char *crust_t;

typedef int (*prueba)(crust_t, int);

#define NULL ((void *)0)

int function(crust_t, int);

void main(void) {
	prueba test1 = function; // All fine, because both function signatures match
}
