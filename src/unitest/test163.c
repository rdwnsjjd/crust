typedef __crust__ unsigned char *crust_t;

#define NULL ((void *)0)

crust_t function1(void);

void main(void) {

	__crust_borrow__ crust_t var1;

	var1 = function1(); // ERROR: assigning a non-borrowed block to a borrow variable
}
