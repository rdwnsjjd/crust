typedef __crust__ unsigned char *crust_t;

typedef int (*prueba)(int);

#define NULL ((void *)0)

int function(crust_t, int);

void main(void) {
	prueba test1 = function; // ERROR: the number of parameters is different, and there are crust-type parameters involved (the first parameter)
}
