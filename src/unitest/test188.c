typedef __crust__ unsigned char *crust_t;

crust_t var1;
crust_t *var2;

void function(void) {

    if (var1 == NULL) {
        __crust_debug__
        var1 = (crust_t)var2;
        __crust_debug__
        var2++;
    }
}
