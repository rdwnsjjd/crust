typedef __crust__ unsigned char *crust_t;

#define NULL ((void *)0)

void free_block(crust_t block);

void main(crust_t param) {

	crust_t looper;

	// This loop works fine
	for(looper = param; looper != NULL; looper = tmp) {
		crust_t tmp = looper->next;
		free_block(looper);
	}
}
