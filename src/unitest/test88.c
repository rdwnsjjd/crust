typedef __crust__ unsigned char *crust_t;

#define NULL ((void *)0)

char* function(crust_t);

void main(void) {
	char* (*test1)(crust_t) = function;
	int b = 1;
	(*test1)(b); // ERROR: calling a function pointer with incorrect type of parameters
}
