typedef __crust__ unsigned char *crust_t;

void function2(crust_t param);

void function(crust_t param1, crust_t param2) {
	crust_t param3 = param1;
	param3 = param2; // ERROR: param3 is overwriten by another variable without being NULL
	function2(param3);
}
