typedef __crust__ unsigned char *crust_t;

#define NULL ((void *)0)

void main(crust_t);

void main(crust_t param1, int) { // ERROR: number of parameters differ from previous definition in line 5

	int a = 5;
	main(param1);
}
