typedef __crust__ unsigned char *crust_t;

#define NULL ((void *)0)

crust_t global_var1;
crust_t global_var2;

void function(crust_t param1) {

	if ((global_var1 == NULL) && (global_var2 == NULL)) {
		global_var1 = param1; // It is fine: since we are storing the block from "param3" in a global variable, it is not a dangling pointer
		global_var2 = param1; // It is NOT fine to assign the same block to several global variables
		__crust_debug__
	} else {
		function(param1); // free param1 to avoid an error
	}
}
