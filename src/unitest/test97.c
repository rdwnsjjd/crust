typedef __crust__ unsigned char *crust_t;

#define NULL ((void *)0)

void main(crust_t param) {

	int b = 0;

	while(b == 0) {
		main(param);
	}
	// ERROR: since b is zero, param will never be freed
}
