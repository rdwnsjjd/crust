__crust__ struct t_crust_t {
	int value;
	__crust__ struct t_crust_t *next;
}

typedef struct t_crust_t  *crust_t;

#define NULL ((void *)0)

void main(crust_t param) {

	crust_t __crust_alias__ alias1;
	crust_t __crust_alias__ alias2;

	__crust_debug__ // here, 'param' must be NOT_NULL_OR_NULL, 'alias1', 'alias2' must be UNINITIALIZED
	alias1 = param;
	__crust_debug__ // here, 'param', 'alias1' must be NOT_NULL_OR_NULL and share UID, 'alias2' must be UNINITIALIZED
	alias2 = alias1;
	__crust_debug__ // here, 'param', 'alias1', 'alias2' must be NOT_NULL_OR_NULL and share UID
	if (alias2 != NULL) {
		alias2->next = alias1;
		__crust_debug__ // here, 'param', 'alias1', 'alias2' must be FREED
	}
}
