typedef __crust__ unsigned char *crust_t;

#define NULL ((void *)0)

crust_t main(void) {

	int b = 5;
	if (b == 7) {
		return;
	} else {
		b = 9;
	}
	return NULL;
}
