typedef __crust__ unsigned char *crust_t;

crust_t var1;
crust_t var2;

void main(crust_t __crust_not_null__ arg1) {

    __crust_set_null__(var1);
    __crust_set_null__(var2);

    var1 = arg1;
    var2 = arg1;
    __crust_debug__
    // ERROR: there are two global variables pointing to the same block
}
