typedef __crust__ unsigned char *crust_t;

typedef void (*prueba)(int);

#define NULL ((void *)0)

void function(int);

void main(crust_t param1) {
	prueba test1 = function;
	(*test1)(param1); // ERROR: calling a function pointer with an incorrect type of parameters
}
