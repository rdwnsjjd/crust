typedef __crust__ unsigned char *crust_t;

#define NULL ((void *)0)

void function(crust_t);

void main(crust_t param) {

	crust_t var2 = (crust_t) 5;
	if (!(param == NULL)) {
		param = var2; // ERROR: param is not NULL
		function(param);
	} else {
		function(var2);
	}
}
