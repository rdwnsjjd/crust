typedef __crust__ unsigned char *crust_t;

#define NULL ((void *)0)

int main(crust_t param) {

	return param; // Error: expects a non-crust value, but a crust value is returned
}
