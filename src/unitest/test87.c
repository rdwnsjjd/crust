typedef __crust__ unsigned char *crust_t;

typedef char* (*prueba)(crust_t);

#define NULL ((void *)0)

char* function(crust_t);

void main(void) {
	prueba test1 = function;
	int b = 1;
	(*test1)(b); // ERROR: calling a function pointer with an incorrect type of parameters
}
