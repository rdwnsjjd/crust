#define NULL ((void *)0)

struct crust_ts {
    char var1;
    int var2;
    struct crust_ts *next;
};

typedef __crust__ struct crust_ts* crust_t;

void function2(crust_t);

void function(crust_t var) {

    crust_t tmp;
    for(; var != NULL; var = tmp) {
        tmp = var->next;
        function2(var);
    }
}
