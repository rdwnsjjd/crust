#!/usr/bin/env python3

# Copyright 2017 (C) Raster Software Vigo (Sergio Costas)
#
# This file is part of CRUST
#
# CRUST is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License.
#
# CRUST is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

import sys

token_list = []

with open(sys.argv[1],"r") as yacc:
    for line in yacc.readlines():
        line = line.strip()
        if not line.startswith("%token "):
            continue
        tokens = line[7:].split(" ")
        for token in tokens:
            if token.strip() == "":
                continue
            token_list.append(token)

with open("tokens.c","w") as output:
    output.write("char *token_list[] = {\n");
    first = True
    for token in token_list:
        if not first:
            output.write(",\n")
        first = False
        output.write('\t"{:s}"'.format(token))
    output.write("\n};\n")

with open("../crust/tokens.py","w") as output:
    output.write("""#!/usr/bin/env python3

class tokens(object):
    token_list = {
""")
    counter = 258
    for token in token_list:
        output.write("        {:d}:'{:s}',\n".format(counter,token))
        counter += 1
    counter = 258
    for token in token_list:
        output.write("        '{:s}':{:d},\n".format(token,counter))
        counter += 1
    output.write("""    }

    @staticmethod
    def get_token(token):
        if isinstance(token,int):
            if token < 258:
                return "{:c}".format(token)
            else:
                return tokens.token_list[token]
        else:
            return token""")
