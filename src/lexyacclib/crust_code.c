/* Copyright 2017 (C) Raster Software Vigo (Sergio Costas)
 *
 * This file is part of CRUST
 *
 * CRUST is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License.
 *
 * CRUST is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

#include <stdio.h>
#include <unistd.h>
#include "crust_code.h"
#include "lexyacc/c99.lex.h"
#include "lexyacc/c99.yacc.h"

// Contains the types list
struct AST *types_list = NULL;
struct AST *full_tree = NULL;

int debug_line1 = -1;
int debug_line2 = -1;

char *mystrdup(char *data) {
	if (data == NULL) {
		return NULL;
	} else {
		return strdup(data);
	}
}

struct AST * new_leaf(int type, struct AST *template) {

	struct AST *ast = (struct AST *)malloc(sizeof(struct AST));
	memset(ast,0,sizeof(struct AST));
	ast->type = type;

	if (template != NULL) {
		ast->filename = mystrdup(template->filename);
		ast->line = template->line;
	} else {
		ast->filename = NULL;
		ast->line = -1;
	}

	ast->data = NULL;

	ast->right = NULL;
	ast->child1 = NULL;
	ast->child2 = NULL;
	ast->child3 = NULL;
	ast->child4 = NULL;
	ast->condition = NULL;
	ast->next = NULL;
	ast->function_params = NULL;
	ast->return_value = NULL;
	ast->struct_data = NULL;
	ast->bitfield = NULL;
	ast->enum_data = NULL;
	ast->arrays = NULL;
	ast->for_ch1 = NULL;
	ast->for_ch2 = NULL;
	ast->for_ch3 = NULL;

	ast->pointer = 0;
	ast->t_const = false;
	ast->t_restrict = false;
	ast->t_volatile = false;

	ast->t_void = false;
	ast->t_char = false;
	ast->t_short = false;
	ast->t_int = false;
	ast->t_long = false;
	ast->t_longlong = false;
	ast->t_float = false;
	ast->t_double = false;
	ast->t_signed = false;
	ast->t_unsigned = false;
	ast->t_bool = false;
	ast->t_va_list = false;
	ast->t_complex = false;
	ast->t_imaginary = false;
	ast->t_struct = false;
	ast->t_union = false;
	ast->t_enum = false;
	ast->t_extern = false;
	ast->t_typedef = false;
	ast->t_static = false;
	ast->t_auto = false;
	ast->t_register = false;
	ast->t_inline = false;
	ast->function = false;
	ast->t_crust = false;
	ast->t_crust_borrow = false;
	ast->t_crust_recycle = false;
	ast->t_crust_alias = false;
	ast->t_crust_no_zero = false;
	ast->t_crust_not_null = false;
	ast->t_null = false;
	ast->t_ellipsis = false;
	ast->t_label = false;
	ast->t_override = false;
	return ast;
}

struct AST * new_leaf_char(int type, char *data) {
	struct AST *element = new_leaf(type,NULL);
	element->data = data;
}

void mix_ast_leaves(struct AST *base, struct AST *origin_leaf) {

	base->pointer += origin_leaf->pointer;
	base->t_const |= origin_leaf->t_const;
	base->t_restrict |= origin_leaf->t_restrict;
	base->t_volatile |= origin_leaf->t_volatile;
	base->t_void |= origin_leaf->t_void;
	base->t_char |= origin_leaf->t_char;
	base->t_short |= origin_leaf->t_short;
	base->t_int |= origin_leaf->t_int;
	if (base->t_long && origin_leaf->t_long) {
		base->t_long = false;
		base->t_longlong = true;
	} else {
		base->t_long |= origin_leaf->t_long;
	}
	base->t_float |= origin_leaf->t_float;
	base->t_double |= origin_leaf->t_double;
	base->t_signed |= origin_leaf->t_signed;
	base->t_unsigned |= origin_leaf->t_unsigned;
	base->t_bool |= origin_leaf->t_bool;
	base->t_va_list |= origin_leaf->t_va_list;
	base->t_complex |= origin_leaf->t_complex;
	base->t_imaginary |= origin_leaf->t_imaginary;
	base->t_struct |= origin_leaf->t_struct;
	base->t_union |= origin_leaf->t_union;
	base->t_enum |= origin_leaf->t_enum;
	base->t_extern |= origin_leaf->t_extern;
	base->t_typedef |= origin_leaf->t_typedef;
	base->t_static |= origin_leaf->t_static;
	base->t_auto |= origin_leaf->t_auto;
	base->t_register |= origin_leaf->t_register;
	base->t_inline |= origin_leaf->t_inline;
	base->function |= origin_leaf->function;
	base->t_crust |= origin_leaf->t_crust;
	base->t_crust_borrow |= origin_leaf->t_crust_borrow;
	base->t_crust_recycle |= origin_leaf->t_crust_recycle;
	base->t_crust_alias |= origin_leaf->t_crust_alias;
	base->t_crust_no_zero |= origin_leaf->t_crust_no_zero;
	base->t_crust_not_null |= origin_leaf->t_crust_not_null;
	base->t_null |= origin_leaf->t_null;
	base->t_ellipsis |= origin_leaf->t_ellipsis;
	base->t_label |= origin_leaf->t_label;
	base->t_override |= origin_leaf->t_override;
	if (base->function_params == NULL) {
		base->function_params = copy_leaf(origin_leaf->function_params, true);
	}
	if (base->return_value == NULL) {
		base->return_value = copy_leaf(origin_leaf->return_value, false);
	}
	if (base->enum_data == NULL) {
		base->enum_data = copy_leaf(origin_leaf->enum_data, true);
	}
	//base->t_ |= origin_leaf->t_;
}

void copy_ast_data(struct AST *base, struct AST *new_leaf) {

	base->pointer = new_leaf->pointer;
	base->t_const = new_leaf->t_const;
	base->t_restrict = new_leaf->t_restrict;
	base->t_volatile = new_leaf->t_volatile;
	base->t_void = new_leaf->t_void;
	base->t_char = new_leaf->t_char;
	base->t_short = new_leaf->t_short;
	base->t_int = new_leaf->t_int;
	base->t_long = new_leaf->t_long;
	base->t_float = new_leaf->t_float;
	base->t_double = new_leaf->t_double;
	base->t_signed = new_leaf->t_signed;
	base->t_unsigned = new_leaf->t_unsigned;
	base->t_bool = new_leaf->t_bool;
	base->t_va_list = new_leaf->t_va_list;
	base->t_complex = new_leaf->t_complex;
	base->t_imaginary = new_leaf->t_imaginary;
	base->t_struct = new_leaf->t_struct;
	base->t_union = new_leaf->t_union;
	base->t_enum = new_leaf->t_enum;
	base->t_extern = new_leaf->t_extern;
	base->t_typedef = new_leaf->t_typedef;
	base->t_static = new_leaf->t_static;
	base->t_auto = new_leaf->t_auto;
	base->t_register = new_leaf->t_register;
	base->t_inline = new_leaf->t_inline;
	base->function = new_leaf->function;
	base->t_crust = new_leaf->t_crust;
	base->t_crust_borrow = new_leaf->t_crust_borrow;
	base->t_crust_recycle = new_leaf->t_crust_recycle;
	base->t_crust_alias = new_leaf->t_crust_alias;
	base->t_crust_no_zero = new_leaf->t_crust_no_zero;
	base->t_crust_not_null = new_leaf->t_crust_not_null;
	base->t_null = new_leaf->t_null;
	base->t_ellipsis = new_leaf->t_ellipsis;
	base->t_label = new_leaf->t_label;
	base->t_override = new_leaf->t_override;
	//base->t_ |= new_leaf->t_;
}

void append_right(struct AST *leaf, struct AST *right) {
	while (leaf->right != NULL) {
		leaf = leaf->right;
	}
	leaf->right = right;
}

void append_next(struct AST *leaf, struct AST *next) {
	int last_line;
	while (leaf->next != NULL) {
		last_line = leaf->line;
		leaf = leaf->next;
	}
	leaf->next = next;
	if ((next->type == END_BLOCK) && (next->line == -1)) {
		next->line = last_line;
	}
}

void append_next_with_blocks(struct AST *leaf, struct AST *next) {
	if (next->type == START_BLOCK) {
		append_next(leaf,next);
	} else {
		append_next_leaf(leaf,START_BLOCK,next->line);
		append_next(leaf, next);
		append_next_leaf(leaf,END_BLOCK,-1);
	}
}

void append_next_leaf(struct AST *leaf,int type_leaf, int line) {
	struct AST *leaf2 = new_leaf(type_leaf,NULL);
	leaf2->line = line;
	append_next(leaf,leaf2);
}

void append_fparams(struct AST *leaf, struct AST *param) {
	while (leaf->function_params != NULL) {
		leaf = leaf->function_params;
	}
	leaf->function_params = param;
}

struct AST *find_type(char *type) {
	struct AST *element;
	for(element=types_list;element!=NULL;element=element->next) {
		if (0 == strcmp(element->data,type)) {
			return element;
		}
	}
	return NULL;
}

#define COPY_ELEMENT(ELEMENT,DEEP) if (leaf->ELEMENT != NULL) {tmp->ELEMENT = copy_leaf(leaf->ELEMENT,DEEP);}
#define RESET_ELEMENT(ELEMENT) tmp->ELEMENT = NULL;
#define COPY_STRING(ELEMENT) if (leaf->ELEMENT != NULL) {tmp->ELEMENT = mystrdup(leaf->ELEMENT);}

struct AST *copy_leaf(struct AST *leaf, bool deep) {
	if (leaf == NULL) {
		return NULL;
	}
	struct AST *tmp = (struct AST *)malloc(sizeof(struct AST));
	memcpy(tmp,leaf,sizeof(struct AST));
	COPY_STRING(data)
	COPY_STRING(name)
	COPY_STRING(filename)
	if (deep) {
		COPY_ELEMENT(next,true);
	} else {
		RESET_ELEMENT(next)
	}
	RESET_ELEMENT(right)
	RESET_ELEMENT(condition)
	RESET_ELEMENT(child1)
	RESET_ELEMENT(child2)
	RESET_ELEMENT(child3)
	RESET_ELEMENT(child4)
	RESET_ELEMENT(bitfield)
	RESET_ELEMENT(struct_data)
	RESET_ELEMENT(enum_data)
	RESET_ELEMENT(arrays)
	RESET_ELEMENT(assignment)
	RESET_ELEMENT(for_ch1)
	RESET_ELEMENT(for_ch2)
	RESET_ELEMENT(for_ch3)
	COPY_ELEMENT(function_params,true)
	COPY_ELEMENT(return_value,false)
	return (tmp);
}

struct AST *transform_leaf(struct AST *leaf, int newtype) {

	if (leaf->child1 == NULL) {
		leaf->type = newtype;
		return leaf;
	}
	if (leaf->child1->right == NULL) {
		leaf->type = newtype;
		return leaf;
	}
	struct AST *newleaf = new_leaf(newtype,leaf);
	newleaf->child1 = leaf;
	return newleaf;
}

void check_typedef(struct AST *name) {

	if ((name == NULL) || (!name->t_typedef)) {
		return;
	}

	struct AST *leaf = copy_leaf(name,false);
	if (find_type(leaf->data) != NULL) {
		printf("Repetido %s",leaf->data);
		exit(-1);
	}

	leaf->next = types_list;
	types_list = leaf;
}


void set_code_tree(struct AST *tree) {
	full_tree = tree;
}

void free_all() {
	free(current_file);
	free_tree(full_tree);
	free(previous_data_line);
	free(current_data_line);
	struct AST *to_delete;
	while(types_list != NULL) {
		to_delete = types_list->next;
		free(types_list);
		types_list = to_delete;
	}

	types_list = NULL;
	full_tree = NULL;
	previous_data_line = NULL;
	current_data_line = NULL;
	int current_data_length = 0;
	current_file = NULL;
	column = 0;
	current_line = 1;
}

void check_tree(struct AST *tree) {

	if (tree == NULL) {
		return;
	}

	struct AST *node;
	for(;tree != NULL; tree = tree->next) {
		check_tree(tree->child1);
		check_tree(tree->child2);
		check_tree(tree->child3);
		check_tree(tree->child4);
		check_tree(tree->condition);
		check_tree(tree->bitfield);
		check_tree(tree->struct_data);
		check_tree(tree->enum_data);
		check_tree(tree->arrays);
		check_tree(tree->assignment);
		check_tree(tree->for_ch1);
		check_tree(tree->for_ch2);
		check_tree(tree->for_ch3);
		check_tree(tree->right);
		check_tree(tree->function_params);
		check_tree(tree->return_value);
	}
}

void free_tree(struct AST *tree) {

	struct AST *to_delete;
	while(tree != NULL) {
		free(tree->filename);
		free(tree->data);
		free(tree->name);
		free_tree(tree->child1);
		free_tree(tree->child2);
		free_tree(tree->child3);
		free_tree(tree->child4);
		free_tree(tree->right);
		free_tree(tree->condition);
		free_tree(tree->function_params);
		free_tree(tree->return_value);
		free_tree(tree->bitfield);
		free_tree(tree->struct_data);
		free_tree(tree->enum_data);
		free_tree(tree->arrays);
		free_tree(tree->assignment);
		free_tree(tree->for_ch1);
		free_tree(tree->for_ch2);
		free_tree(tree->for_ch3);
		to_delete = tree;
		tree = tree->next;
		free(to_delete);
	}
}

void show_error(int line,struct AST *element) {
	print_lines();
	printf("\n\nUndefined Statement at line %d; Data: %s; File: %s\n\nPlease, contact the author.\n\n",line,element->data,element->filename);
	exit(-1);
}

void show_debug(int line, struct AST *element, char *node) {
	if ((current_line >= debug_line1) && (current_line <= debug_line2)) {
		printf("Changed to type %s at line %d (code line: %d, file: %s; code data: %s)\n",node,line,current_line,current_file,element->data);
	}
}

struct AST* parse_data(bool show_debug, char *filename, void *data, int size) {
	do_verbose = show_debug ? 1 : 0;
	types_list = NULL;
	full_tree = NULL;
	current_file = mystrdup(filename);
	yyin = fmemopen(data,size,"r");
	if (yyparse() == 0) {
		return full_tree;
	} else {
		return NULL;
	}
}

void set_debug_values(int first_line, int last_line) {
	debug_line1 = first_line;
	debug_line2 = last_line;
}

#ifdef LIBRARY
struct AST* parse_file(bool show_debug, char *filename) {
	do_verbose = show_debug ? 1 : 0;
#else
int main(int argc,char **argv) {
	do_verbose = 0;
	char *filename = argv[1];
#endif
	int retval = -1;
	types_list = NULL;
	full_tree = NULL;
	current_file = mystrdup(filename);

	yyin = fopen(filename,"r");

	if (yyin != NULL) {
		retval = yyparse();
	}
	fclose(yyin);
	printf("Error: %d\n",retval);
#ifdef LIBRARY
	if (retval == 0){
		return (full_tree);
	} else {
		return (NULL);
	}
#else
	check_tree(full_tree);
	free_all();
	return retval;
#endif
}
