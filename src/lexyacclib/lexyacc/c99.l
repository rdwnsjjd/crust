%{
/*
 * Original rules written by Jeff Lee and published by Tom Stockfisch in Usenet
 * and currently maintained by Jutta Degener
 *
 * Changes for CRUST: Copyright 2017 (C) Raster Software Vigo (Sergio Costas)
 *
 * This file is part of CRUST
 *
 * CRUST is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License.
 *
 * CRUST is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 *
 *
 */
%}

D			[0-9]
L			[a-zA-Z_]
H			[a-fA-F0-9]
E			([Ee][+-]?{D}+)
P                       ([Pp][+-]?{D}+)
FS			(f|F|l|L)
IS                      ((u|U)|(u|U)?(l|L|ll|LL)|(l|L|ll|LL)(u|U))
WS          [ \\t\\v\\n\\f]
WS2         [ \\t]

%{
#include <stdio.h>
#include "c99.yacc.h"
#include "../crust_code.h"

int tmptype;
unsigned long long int tmpint;
double tmpfloat;
void comment(void);
void error(char *);
void count();
void set_current_line();
char *previous_data_line = NULL;
char *current_data_line = NULL;
int current_data_length = 0;

int column = 0;
int current_line = 1;
char *current_file = NULL;
char do_verbose = 0;

void find_close_parentheses(int, bool);
%}


%%
"/*"										{ comment(); }
"//"[^\n]*									{ current_line++; /* consume //-comment */ }
"#pragma"[^\n]*								{ count(); }
\#{WS2}+[0-9]+{WS2}+\".+\"({WS2}+[0-9]+)*	{ set_current_line(); }

"auto"										{ return prepare_leaf(0,0.0,AUTO); }
"_Bool"										{ return prepare_leaf(0,0.0,BOOL); }
"break"										{ return prepare_leaf(0,0.0,BREAK); }
"case"										{ return prepare_leaf(0,0.0,CASE); }
"char"										{ return prepare_leaf(0,0.0,CHAR); }
"_Complex"									{ return prepare_leaf(0,0.0,COMPLEX); }
"const"										{ return prepare_leaf(0,0.0,CONST); }
"continue"									{ return prepare_leaf(0,0.0,CONTINUE); }
"default"									{ return prepare_leaf(0,0.0,DEFAULT); }
"do"										{ return prepare_leaf(0,0.0,DO); }
"double"									{ return prepare_leaf(0,0.0,DOUBLE); }
"else"										{ return prepare_leaf(0,0.0,ELSE); }
"enum"										{ return prepare_leaf(0,0.0,ENUM); }
"extern"									{ return prepare_leaf(0,0.0,EXTERN); }
"float"										{ return prepare_leaf(0,0.0,FLOAT); }
"for"										{ return prepare_leaf(0,0.0,FOR); }
"goto"										{ return prepare_leaf(0,0.0,GOTO); }
"if"										{ return prepare_leaf(0,0.0,IF); }
"_Imaginary"								{ return prepare_leaf(0,0.0,IMAGINARY); }
"inline"									{ return prepare_leaf(0,0.0,INLINE); }
"int"										{ return prepare_leaf(0,0.0,INT); }
"long"										{ return prepare_leaf(0,0.0,LONG); }
"register"									{ return prepare_leaf(0,0.0,REGISTER); }
"restrict"									{ return prepare_leaf(0,0.0,RESTRICT); }
"return"									{ return prepare_leaf(0,0.0,RETURN); }
"short"										{ return prepare_leaf(0,0.0,SHORT); }
"signed"									{ return prepare_leaf(0,0.0,SIGNED); }
"sizeof"									{ return prepare_leaf(0,0.0,SIZEOF); }
"static"									{ return prepare_leaf(0,0.0,STATIC); }
"struct"									{ return prepare_leaf(0,0.0,STRUCT); }
"switch"									{ return prepare_leaf(0,0.0,SWITCH); }
"typedef"									{ return prepare_leaf(0,0.0,TYPEDEF); }
"union"										{ return prepare_leaf(0,0.0,UNION); }
"unsigned"									{ return prepare_leaf(0,0.0,UNSIGNED); }
"void"										{ return prepare_leaf(0,0.0,VOID); }
"volatile"									{ return prepare_leaf(0,0.0,VOLATILE); }
"while"										{ return prepare_leaf(0,0.0,WHILE); }
"NULL"										{ return prepare_leaf(0,0.0,T_NULL); }
"__typeof__"						        { return prepare_leaf(0,0.0,T_TYPEOF); }
"typeof"    						        { return prepare_leaf(0,0.0,T_TYPEOF); }
"__builtin_va_list"							{ return prepare_leaf(0,0.0,VA_LIST); }
"__builtin_offsetof"{WS}*"("    			{ find_close_parentheses(1, false); count(); return prepare_leaf(0,0.0,CONSTANT); }
"__signed__"                                { return prepare_leaf(0,0.0,SIGNED); }
"__extension__"								{ count(); }
"__prog__"									{ count(); }
"__restrict"								{ count(); }
"__inline"									{ count(); }
"__attribute__"{WS}*"("     				{ find_close_parentheses(1, false); count(); }
"_Static_assert"{WS}*"("			        { find_close_parentheses(1, true); count(); }
"__asm__"({WS}+[a-zA-Z_]+)*{WS}*"("    	    { find_close_parentheses(1, false); count(); }
"__asm"({WS}+[a-zA-Z_]+)*{WS}*"("    	    { find_close_parentheses(1, false); count(); }
"asm"({WS}+[a-zA-Z_]+)*{WS}*"("        	    { find_close_parentheses(1, false); count(); }
"__alignof__("                              { find_close_parentheses(1, false); count(); return prepare_leaf(1,0.0,CONSTANT); }
"__crust__"									{ return prepare_leaf(0,0.0,CRUST_T); }
"__crust_borrow__"							{ return prepare_leaf(0,0.0,CRUST_BORROW); }
"__crust_recycle__"							{ return prepare_leaf(0,0.0,CRUST_RECYCLE); }
"__crust_alias__"							{ return prepare_leaf(0,0.0,CRUST_ALIAS); }
"__crust_enable__"							{ return prepare_leaf(0,0.0,CRUST_ENABLE); }
"__crust_disable__"							{ return prepare_leaf(0,0.0,CRUST_DISABLE); }
"__crust_full_enable__"						{ return prepare_leaf(0,0.0,CRUST_FULL_ENABLE); }
"__crust_debug__"							{ return prepare_leaf(0,0.0,CRUST_DEBUG); }
"__crust_no_0__"							{ return prepare_leaf(0,0.0,CRUST_NO_ZERO); }
"__crust_not_null__"						{ return prepare_leaf(0,0.0,CRUST_NOT_NULL); }
"__crust_set_null__"                        { return prepare_leaf(0,0.0,CRUST_SET_NULL); }
"__crust_set_not_null__"                    { return prepare_leaf(0,0.0,CRUST_SET_NOT_NULL); }
"__crust_override__"                        { return prepare_leaf(0,0.0,CRUST_OVERRIDE); }
"__crust_no_warning__"                      { return prepare_leaf(0,0.0,CRUST_NO_WARNING); }


{L}({L}|{D})*								{ return prepare_leaf(0,0.0,TYPENAME_IDENTIFIER); }

0[xX]{H}+{IS}?								{ sscanf(yytext,"%Lx",&tmpint); return prepare_leaf(tmpint,0.0,CONSTANT); }
0[0-7]*{IS}?								{ sscanf(yytext,"%Lo",&tmpint); return prepare_leaf(tmpint,0.0,CONSTANT); }
[1-9]{D}*{IS}?								{ sscanf(yytext,"%Lu",&tmpint); return prepare_leaf(tmpint,0.0,CONSTANT); }
L?'(\\.|[^\\'\n])+'							{	char *start;
												unsigned long long int value;
												if (yytext[0] == '\'') {
													start = yytext + 1;
												} else {
													start = yytext + 2;
												}
												if (*start == '\\') {
													switch (*(start+1)) {
													case 'n':
														value = 10;
														break;
													case 'r':
														value = 13;
														break;
													case 't':
														value = 9;
														break;
													case 'b':
														value = 8;
														break;
													case '0':
														value = 0;
														break;
													default:
														value = (unsigned long long int) (*(start+1));
														break;
													}
												} else {
													value = (unsigned long long int) (*start);
												}
												return prepare_leaf(value,0.0,CONSTANT);
											}

{D}+{E}{FS}?								{ sscanf(yytext,"%lf",&tmpfloat); return prepare_leaf(0,tmpfloat,FCONSTANT); }
{D}*"."{D}+{E}?{FS}?						{ sscanf(yytext,"%lf",&tmpfloat); return prepare_leaf(0,tmpfloat,FCONSTANT); }
{D}+"."{D}*{E}?{FS}?						{ sscanf(yytext,"%lf",&tmpfloat); return prepare_leaf(0,tmpfloat,FCONSTANT); }
0[xX]{H}+{P}{FS}?							{ sscanf(yytext,"%lfx",&tmpfloat); return prepare_leaf(0,tmpfloat,FCONSTANT); }
0[xX]{H}*"."{H}+{P}{FS}?					{ sscanf(yytext,"%lfx",&tmpfloat); return prepare_leaf(0,tmpfloat,FCONSTANT); }
0[xX]{H}+"."{H}*{P}{FS}?					{ sscanf(yytext,"%lfx",&tmpfloat);  return prepare_leaf(0,tmpfloat,FCONSTANT); }


L?\"(\\.|[^\\"\n])*\"						{ return prepare_leaf(0,0.0,STRING_LITERAL); }

"..."										{ return prepare_leaf(0,0.0,ELLIPSIS); }
">>="										{ return prepare_leaf(0,0.0,RIGHT_ASSIGN); }
"<<="										{ return prepare_leaf(0,0.0,LEFT_ASSIGN); }
"+="										{ return prepare_leaf(0,0.0,ADD_ASSIGN); }
"-="										{ return prepare_leaf(0,0.0,SUB_ASSIGN); }
"*="										{ return prepare_leaf(0,0.0,MUL_ASSIGN); }
"/="										{ return prepare_leaf(0,0.0,DIV_ASSIGN); }
"%="										{ return prepare_leaf(0,0.0,MOD_ASSIGN); }
"&="										{ return prepare_leaf(0,0.0,AND_ASSIGN); }
"^="										{ return prepare_leaf(0,0.0,XOR_ASSIGN); }
"|="										{ return prepare_leaf(0,0.0,OR_ASSIGN); }
">>"										{ return prepare_leaf(0,0.0,RIGHT_OP); }
"<<"										{ return prepare_leaf(0,0.0,LEFT_OP); }
"++"										{ return prepare_leaf(0,0.0,INC_OP); }
"--"										{ return prepare_leaf(0,0.0,DEC_OP); }
"->"										{ return prepare_leaf(0,0.0,PTR_OP); }
"&&"										{ return prepare_leaf(0,0.0,AND_OP); }
"||"										{ return prepare_leaf(0,0.0,OR_OP); }
"<="										{ return prepare_leaf(0,0.0,LE_OP); }
">="										{ return prepare_leaf(0,0.0,GE_OP); }
"=="										{ return prepare_leaf(0,0.0,EQ_OP); }
"!="										{ return prepare_leaf(0,0.0,NE_OP); }
";"											{ return prepare_leaf(0,0.0,';'); }
("{"|"<%")									{ return prepare_leaf(0,0.0,'{'); }
("}"|"%>")									{ return prepare_leaf(0,0.0,'}'); }
","											{ return prepare_leaf(0,0.0,','); }
":"											{ return prepare_leaf(0,0.0,':'); }
"="											{ return prepare_leaf(0,0.0,'='); }
"("											{ return prepare_leaf(0,0.0,'('); }
")"											{ return prepare_leaf(0,0.0,')'); }
("["|"<:")									{ return prepare_leaf(0,0.0,'['); }
("]"|":>")									{ return prepare_leaf(0,0.0,']'); }
"."											{ return prepare_leaf(0,0.0,'.'); }
"&"											{ return prepare_leaf(0,0.0,'&'); }
"!"											{ return prepare_leaf(0,0.0,'!'); }
"~"											{ return prepare_leaf(0,0.0,'~'); }
"-"											{ return prepare_leaf(0,0.0,'-'); }
"+"											{ return prepare_leaf(0,0.0,'+'); }
"*"											{ return prepare_leaf(0,0.0,'*'); }
"/"											{ return prepare_leaf(0,0.0,'/'); }
"%"											{ return prepare_leaf(0,0.0,'%'); }
"<"											{ return prepare_leaf(0,0.0,'<'); }
">"											{ return prepare_leaf(0,0.0,'>'); }
"^"											{ return prepare_leaf(0,0.0,'^'); }
"|"											{ return prepare_leaf(0,0.0,'|'); }
"?"											{ return prepare_leaf(0,0.0,'?'); }

[ \t\v\n\f]									{ count(); }
						.					{ /* Add code to complain about unmatched characters */ }

%%

int yywrap(void)
{
	return 1;
}


void set_current_line() {

	char *pos = yytext+2;
	char *start;
	int size;

	free(current_file);
	current_file = NULL;
	sscanf(yytext,("# %d"),&current_line);
	current_line--;
	while((*pos != '"') && (*pos != 0)) {
		pos++;
	}
	if (*pos == 0) {
		return;
	}
	pos++;
	start = pos;
	size = 0;
	while((*pos != '"') && (*pos != 0)) {
		pos++;
		size++;
	}
	if (*pos == 0) {
		return;
	}
	current_file = (char *)malloc(size+1);
	memcpy(current_file,start,size);
	current_file[size] = 0;
}

void count() {

	int i;

	int j = current_data_length + strlen(yytext);
	if (current_data_line == NULL) {
		current_data_line = malloc(j + 1);
		current_data_line[0] = 0;
	} else {
		current_data_line = realloc(current_data_line, j + 1);
	}
	current_data_length = j;
	strcat(current_data_line,yytext);

	for (i = 0; yytext[i] != '\0'; i++)
		if (yytext[i] == '\n') {
			column = 0;
			current_line++;
			free(previous_data_line);
			previous_data_line = current_data_line;
			current_data_line = NULL;
			current_data_length = 0;
		} else if (yytext[i] == '\t')
			column += 8 - (column % 8);
		else
			column++;
	if (do_verbose) {
		ECHO;
	}
}

int prepare_leaf(unsigned long long int v1, double v2, int current_token) {

	char *data;
	struct AST *leaf2;
	if (yytext == NULL) {
		data = NULL;
	} else {
		data = strdup(yytext);
	}
	yylval = new_leaf_char(current_token, data);
	yylval->intval = v1;
	yylval->floatval = v2;

	switch(current_token) {
		case VOID:
			yylval->t_void = true;
		break;
		case CHAR:
			yylval->t_char = true;
		break;
		case SHORT:
			yylval->t_short = true;
		break;
		case INT:
			yylval->t_int = true;
		break;
		case LONG:
			yylval->t_long = true;
		break;
		case FLOAT:
			yylval->t_float = true;
		break;
		case DOUBLE:
			yylval->t_double = true;
		break;
		case SIGNED:
			yylval->t_signed = true;
		break;
		case UNSIGNED:
			yylval->t_unsigned = true;
		break;
		case BOOL:
			yylval->t_bool = true;
		break;
		case VA_LIST:
			yylval->t_va_list = true;
		break;
		case COMPLEX:
			yylval->t_complex = true;
		break;
		case IMAGINARY:
			yylval->t_imaginary = true;
		break;
		case STRUCT:
			yylval->t_struct = true;
		break;
		case ENUM:
			yylval->t_enum = true;
		break;
		case INLINE:
			yylval->t_inline = true;
		break;
		case CONST:
			yylval->t_const = true;
		break;
		case RESTRICT:
			yylval->t_restrict = true;
		break;
		case VOLATILE:
			yylval->t_volatile = true;
		break;
		case TYPEDEF:
			yylval->t_typedef = true;
		break;
		case EXTERN:
			yylval->t_extern = true;
		break;
		case STATIC:
			yylval->t_static = true;
		break;
		case AUTO:
			yylval->t_auto = true;
		break;
		case REGISTER:
			yylval->t_register = true;
		break;
		case CRUST_T:
			yylval->t_crust = true;
		break;
		case CRUST_BORROW:
			yylval->t_crust_borrow = true;
			yylval->t_crust = true;
		break;
		case CRUST_RECYCLE:
			yylval->t_crust_recycle = true;
			yylval->t_crust = true;
		break;
		case CRUST_ALIAS:
			yylval->t_crust_alias = true;
			yylval->t_crust = true;
		break;
		case CRUST_NOT_NULL:
			yylval->t_crust_not_null = true;
		break;
        case CRUST_OVERRIDE:
            yylval->t_override = true;
        break;
		case T_NULL:
			yylval->t_null = true;
			free(yylval->data);
			yylval->data = strdup("0");
			yylval->intval = 0;
			current_token = CONSTANT;
			yylval->type = current_token;
		break;
		case TYPENAME_IDENTIFIER:
			leaf2 = find_type(yytext);
			if (leaf2 != NULL) {
				mix_ast_leaves(yylval,leaf2);
				yylval->t_typedef = false;
				current_token = TYPE_NAME;
			} else {
				current_token = IDENTIFIER;
			}
			yylval->type = current_token;
		break;
	}

	yylval->line = current_line;
	if (current_file != NULL) {
		yylval->filename = strdup(current_file);
	} else {
		yylval->filename = NULL;
	}
	count();
	return current_token;
}

void find_close_parentheses(int init_counter, bool remove_semicolon) {

    char c;
    int counter = init_counter;
    bool inside_quotes = false;

    while ((counter != 0) && ((c = input()) != 0)) {
        if (c == '\n') {
            current_line++;
            continue;
        }
        if (c == '\'') {
            c = input();
            if (c == '\\') {
                c = input();
            }
            c = input();
            continue;
        }
        if (inside_quotes) {
            if (c == '\\') {
                input();
                continue;
            }
            if (c == '"') {
                inside_quotes = false;
            }
            continue;
        }
        if (c == '"') {
            inside_quotes = true;
        }
        if (c == '(') {
            counter++;
            continue;
        }
        if (c == ')') {
            counter--;
            continue;
        }
    }
    if (remove_semicolon) {
        do {
            c = input();
        } while(c != ';');
    }
}

void comment(void) {
	char c, prev = 0;

	while ((c = input()) != 0) {      /* (EOF maps to 0) */
        if (c == '\n') {
            current_line++;
            continue;
        }
		if (c == '/' && prev == '*')
			return;
		prev = c;
	}
	error("unterminated comment");
}
