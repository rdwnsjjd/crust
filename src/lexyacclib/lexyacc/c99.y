/*
 * Original rules written by Jeff Lee and published by Tom Stockfisch in Usenet
 * and currently maintained by Jutta Degener
 *
 * Changes for CRUST: Copyright 2017 (C) Raster Software Vigo (Sergio Costas)
 *
 * This file is part of CRUST
 *
 * CRUST is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License.
 *
 * CRUST is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

%{
#include <stdio.h>
#include "../crust_code.h"

extern  int yylex();
extern  int yyparse();
extern  FILE *yyin;
struct AST *tmpleaf;
struct AST *tmpleaf2;
struct AST *tmpleaf3;

void yyerror(const char *s);
void add_type_element(char *);
%}

%define api.value.type {struct AST *}

%token IDENTIFIER CONSTANT STRING_LITERAL SIZEOF
%token PTR_OP INC_OP DEC_OP LEFT_OP RIGHT_OP LE_OP GE_OP EQ_OP NE_OP
%token AND_OP OR_OP MUL_ASSIGN DIV_ASSIGN MOD_ASSIGN ADD_ASSIGN
%token SUB_ASSIGN LEFT_ASSIGN RIGHT_ASSIGN AND_ASSIGN
%token XOR_ASSIGN OR_ASSIGN TYPE_NAME

%token TYPEDEF EXTERN STATIC AUTO REGISTER INLINE RESTRICT
%token CHAR SHORT INT LONG SIGNED UNSIGNED FLOAT DOUBLE CONST VOLATILE VOID
%token BOOL COMPLEX IMAGINARY
%token STRUCT UNION ENUM ELLIPSIS

%token CASE DEFAULT IF ELSE SWITCH WHILE DO FOR GOTO CONTINUE BREAK RETURN

%token VA_LIST
%token T_EXTENSION
%token TYPE_SPECIFIER
%token VARIABLE_DEFINITION
%token T_NULL
%token FCONSTANT
%token EMPTY_DECLARATOR
%token FUNCTION_CALL
%token FUNCTION_DEFINITION
%token FUNCTION_DECLARATION
%token BRACKETS_CONTAINER
%token LABEL
%token START_BLOCK
%token END_BLOCK
%token TYPECAST
%token TYPENAME_IDENTIFIER
%token T_TYPEOF

%token CRUST_T
%token CRUST_BORROW
%token CRUST_RECYCLE
%token CRUST_ALIAS
%token CRUST_NOT_NULL
%token CRUST_ENABLE
%token CRUST_DISABLE
%token CRUST_FULL_ENABLE
%token CRUST_DEBUG
%token CRUST_NO_ZERO
%token CRUST_SET_NULL
%token CRUST_SET_NOT_NULL
%token CRUST_OVERRIDE
%token CRUST_NO_WARNING
%token OWN_POINTER
%token ASSIGN_BLOCK

//%start translation_unit
%start start_unit
%%

start_unit
	: translation_unit	{	$$ = $1;
							show_debug(__LINE__, $$, "start_unit");
							set_code_tree($1); }
	;

primary_expression
	: IDENTIFIER			{	$$ = $1;
								show_debug(__LINE__, $$, "primary_expression");
								$1->name = mystrdup($1->data);
							}
	| CONSTANT				{	$$ = $1; if ($$->intval == 0) {$$->t_null = true;} show_debug(__LINE__, $$, "primary_expression"); }
	| FCONSTANT				{	$$ = $1; show_debug(__LINE__, $$, "primary_expression"); }
	| long_string_literal	{	$$ = $1; show_debug(__LINE__, $$, "primary_expression"); }
	| '(' expression ')'	{	$$ = $2; show_debug(__LINE__, $$, "primary_expression");
								free_tree($1);
								free_tree($3); }
	;

long_string_literal
	: STRING_LITERAL						{	$$ = $1; show_debug(__LINE__, $$, "long_string_literal"); }
	| long_string_literal STRING_LITERAL	{	/* support for "xxxxx""yyy" strings */
                                                int size1 = strlen($1->data);
												char *data = (char *)malloc(size1 + strlen($2->data) - 1);
												strcpy(data, $1->data);
												data[size1 - 1] = 0; // remove the last double quote
												strcat(data, $2->data+1); // remove the first double quote
												free($1->data);
												$1->data = data;
												free_tree($2);
												$$ = $1; show_debug(__LINE__, $$, "long_string_literal");
											}



postfix_expression
	: primary_expression									{	$$ = $1; show_debug(__LINE__, $$, "postfix_expression"); }
	| postfix_expression '[' expression ']'					{	$$ = $1; show_debug(__LINE__, $$, "postfix_expression");
																tmpleaf = new_leaf(BRACKETS_CONTAINER, $3);
																append_right($$,tmpleaf);
																tmpleaf->child1 = $3;
																free_tree($2);
																free_tree($4); }
	| postfix_expression '(' ')'							{	// calling a function without parameters
																if (($1->type == '*') && ($1->child1 != NULL) && ($1->child1->type == IDENTIFIER)) {
																	$$ = $1->child1;
																	$1->child1 = NULL;
																} else {
																	$$ = $1;
																}
																$$->type = FUNCTION_CALL;
																$$->function_params = new_leaf(EMPTY_DECLARATOR, $2);
																show_debug(__LINE__, $$, "posftix_expression");
																free_tree($2);
																free_tree($3); }
	| postfix_expression '(' argument_expression_list ')'	{	// calling a function with parameters
																if (($1->type == '*') && ($1->child1 != NULL) && ($1->child1->type == IDENTIFIER)) {
																	$$ = $1->child1;
																	$1->child1 = NULL;
																} else {
																	$$ = $1;
																}
																$$->type = FUNCTION_CALL;
																$$->function_params = $3;
																show_debug(__LINE__, $$, "posftix_expression");
																free_tree($2);
																free_tree($4); }
	| postfix_expression '.' IDENTIFIER						{	$$ = $1; show_debug(__LINE__, $$, "posftix_expression");
																append_right($$, $2);
																$2->child1 = $3; }
	| postfix_expression PTR_OP IDENTIFIER					{	$$ = $1; show_debug(__LINE__, $$, "posftix_expression");
																append_right($$, $2);
																$2->child1 = $3; }
	| postfix_expression INC_OP								{	$$ = $2; show_debug(__LINE__, $$, "posftix_expression");
																$$->child1 = $1; }
	| postfix_expression DEC_OP								{	$$ = $2; show_debug(__LINE__, $$, "posftix_expression");
																$$->child1 = $1; }
	| '(' type_name ')' '{' initializer_list '}'			{	$$ = $5; show_debug(__LINE__, $$, "posftix_expression");
                                                                mix_ast_leaves($5, $2);
                                                                free_tree($1);
                                                                free_tree($2);
                                                                free_tree($3);
                                                                free_tree($4);
                                                                free_tree($6); }
	| '(' type_name ')' '{' initializer_list ',' '}'		{	$$ = $5; show_debug(__LINE__, $$, "posftix_expression");
                                                                mix_ast_leaves($5, $2);
                                                                free_tree($1);
                                                                free_tree($2);
                                                                free_tree($3);
                                                                free_tree($4);
                                                                free_tree($6);
                                                                free_tree($7); }
	;

argument_expression_list
	: assignment_expression									{	$$ = $1;show_debug(__LINE__, $$, "argument_expression_list"); }
	| argument_expression_list ',' assignment_expression	{	$$ = $1;show_debug(__LINE__, $$, "argument_expression_list");
																append_next($$, $3);
																free_tree($2); }
	;

unary_expression
	: postfix_expression				{	$$ = $1;show_debug(__LINE__, $$, "unary_expression"); }
	| INC_OP unary_expression			{	$$ = $1;show_debug(__LINE__, $$, "unary_expression");
											$$->child2 = $2; }
	| DEC_OP unary_expression			{	$$ = $1;show_debug(__LINE__, $$, "unary_expression");
											$$->child2 = $2; }
	| unary_operator cast_expression	{	$$ = $1;show_debug(__LINE__, $$, "unary_expression");
											$$->child1 = $2; }
	| SIZEOF unary_expression			{	$$ = $1;show_debug(__LINE__, $$, "unary_expression");
											$$->child1 = $2; }
	| SIZEOF '(' type_name ')'			{	$$ = $1;show_debug(__LINE__, $$, "unary_expression");
											$$->child1 = $3;
											free_tree($2);
											free_tree($4); }
	;

unary_operator
	: '&'								{ $$ = $1; show_debug(__LINE__, $$, "unary_operator"); }
	| '*'								{ $$ = $1; show_debug(__LINE__, $$, "unary_operator"); }
	| '+'								{ $$ = $1; show_debug(__LINE__, $$, "unary_operator"); }
	| '-'								{ $$ = $1; show_debug(__LINE__, $$, "unary_operator"); }
	| '~'								{ $$ = $1; show_debug(__LINE__, $$, "unary_operator"); }
	| '!'								{ $$ = $1; show_debug(__LINE__, $$, "unary_operator"); }
	;

cast_expression
	: unary_expression					{	$$ = $1; show_debug(__LINE__, $$, "cast_expression"); }
	| '(' type_name ')' cast_expression	{	$$ = $2; show_debug(__LINE__, $$, "cast_expression");
											$$->type = TYPECAST;
											$$->child1 = $4;
											if (($4->type == CONSTANT) && ($4->intval == 0) && ($2->pointer == 1) && ($2->t_void)) {
												$$->t_null = true;
											}
											free_tree($1);
											free_tree($3);
										}
	;

multiplicative_expression
	: cast_expression								{	$$ = $1; show_debug(__LINE__, $$, "multiplicative_expression"); }
	| multiplicative_expression '*' cast_expression {	$$ = $2; show_debug(__LINE__, $$, "multiplicative_expression");
														$$->child1 = $1;
														$$->child2 = $3; }
	| multiplicative_expression '/' cast_expression {	$$ = $2; show_debug(__LINE__, $$, "multiplicative_expression");
														$$->child1 = $1;
														$$->child2 = $3; }
	| multiplicative_expression '%' cast_expression {	$$ = $2; show_debug(__LINE__, $$, "multiplicative_expression");
														$$->child1 = $1;
														$$->child2 = $3; }
	;

additive_expression
	: multiplicative_expression							{	$$ = $1; show_debug(__LINE__, $$, "additive_expression"); }
	| additive_expression '+' multiplicative_expression {	$$ = $2; show_debug(__LINE__, $$, "additive_expression");
															$$->child1 = $1;
															$$->child2 = $3; }
	| additive_expression '-' multiplicative_expression {	$$ = $2; show_debug(__LINE__, $$, "additive_expression");
															$$->child1 = $1;
															$$->child2 = $3; }
	;

shift_expression
	: additive_expression							{	$$ = $1; show_debug(__LINE__, $$, "shift_expression"); }
	| shift_expression LEFT_OP additive_expression	{	$$ = $2; show_debug(__LINE__, $$, "shift_expression");
														$$->child1 = $1;
														$$->child2 = $3; }
	| shift_expression RIGHT_OP additive_expression {	$$ = $2; show_debug(__LINE__, $$, "shift_expression");
														$$->child1 = $1;
														$$->child2 = $3; }
	;

relational_expression
	: shift_expression								{	$$ = $1; show_debug(__LINE__, $$, "relational_expression"); }
	| relational_expression '<' shift_expression	{	$$ = $2; show_debug(__LINE__, $$, "relational_expression");
														$$->child1 = $1;
														$$->child2 = $3; }
	| relational_expression '>' shift_expression	{	$$ = $2; show_debug(__LINE__, $$, "relational_expression");
														$$->child1 = $1;
														$$->child2 = $3; }
	| relational_expression LE_OP shift_expression	{	$$ = $2; show_debug(__LINE__, $$, "relational_expression");
														$$->child1 = $1;
														$$->child2 = $3; }
	| relational_expression GE_OP shift_expression	{	$$ = $2; show_debug(__LINE__, $$, "relational_expression");
														$$->child1 = $1;
														$$->child2 = $3; }
	;

equality_expression
	: relational_expression								{	$$ = $1; show_debug(__LINE__, $$, "equality_expression"); }
	| equality_expression EQ_OP relational_expression	{	$$ = $2; show_debug(__LINE__, $$, "equality_expression");
															$$->child1 = $1;
															$$->child2 = $3; }
	| equality_expression NE_OP relational_expression	{	$$ = $2; show_debug(__LINE__, $$, "equality_expression");
															$$->child1 = $1;
															$$->child2 = $3; }
	;

and_expression
	: equality_expression						{	$$ = $1; show_debug(__LINE__, $$, "and_expression"); }
	| and_expression '&' equality_expression	{	$$ = $2; show_debug(__LINE__, $$, "and_expression");
													$$->child1 = $1;
													$$->child2 = $3; }
	;

exclusive_or_expression
	: and_expression								{	$$ = $1; show_debug(__LINE__, $$, "xor_expression"); }
	| exclusive_or_expression '^' and_expression	{	$$ = $2; show_debug(__LINE__, $$, "xor_expression");
														$$->child1 = $1;
														$$->child2 = $3; }
	;

inclusive_or_expression
	: exclusive_or_expression								{	$$ = $1; show_debug(__LINE__, $$, "or_expression"); }
	| inclusive_or_expression '|' exclusive_or_expression	{	$$ = $2; show_debug(__LINE__, $$, "or_expression");
																$$->child1 = $1;
																$$->child2 = $3; }
	;

logical_and_expression
	: inclusive_or_expression								{	$$ = $1; show_debug(__LINE__, $$, "logical_and_expression"); }
	| logical_and_expression AND_OP inclusive_or_expression	{	$$ = $2; show_debug(__LINE__, $$, "logical_and_expression");
																$$->child1 = $1;
																$$->child2 = $3; }
	;

logical_or_expression
	: logical_and_expression								{	$$ = $1; show_debug(__LINE__, $$, "logical_or_expression"); }
	| logical_or_expression OR_OP logical_and_expression	{	$$ = $2; show_debug(__LINE__, $$, "logical_or_expression");
																$$->child1 = $1;
																$$->child2 = $3; }
	;

conditional_expression
	: logical_or_expression												{	$$ = $1; show_debug(__LINE__, $$, "conditional_expression"); }
	| logical_or_expression '?' expression ':' conditional_expression	{	$$ = $2; show_debug(__LINE__, $$, "conditional_expression");
																			$$->condition = $1;
																			$$->child1 = $3;
																			$$->child2 = $5;
																			free_tree($4); }
	;

assignment_expression
	: conditional_expression										{	$$ = $1; show_debug(__LINE__, $$, "assignment_expression"); }
	| unary_expression assignment_operator assignment_expression	{	$$ = $2; show_debug(__LINE__, $$, "assignment_expression");
																		$$->child1 = $1;
																		$$->child2 = $3; }
    | '(' compound_statement ')'                                    {   $$ = $1;
                                                                        $1->type = ASSIGN_BLOCK;
                                                                        $1->child1 = $2;
                                                                        free_tree($3);
                                                                        show_debug(__LINE__, $$, "assignment_expression"); }
	;

assignment_operator
	: '='				{ $$ = $1; show_debug(__LINE__, $$, "assignment operator"); }
	| MUL_ASSIGN		{ $$ = $1; show_debug(__LINE__, $$, "assignment operator"); }
	| DIV_ASSIGN		{ $$ = $1; show_debug(__LINE__, $$, "assignment operator"); }
	| MOD_ASSIGN		{ $$ = $1; show_debug(__LINE__, $$, "assignment operator"); }
	| ADD_ASSIGN		{ $$ = $1; show_debug(__LINE__, $$, "assignment operator"); }
	| SUB_ASSIGN		{ $$ = $1; show_debug(__LINE__, $$, "assignment operator"); }
	| LEFT_ASSIGN		{ $$ = $1; show_debug(__LINE__, $$, "assignment operator"); }
	| RIGHT_ASSIGN		{ $$ = $1; show_debug(__LINE__, $$, "assignment operator"); }
	| AND_ASSIGN		{ $$ = $1; show_debug(__LINE__, $$, "assignment operator"); }
	| XOR_ASSIGN		{ $$ = $1; show_debug(__LINE__, $$, "assignment operator"); }
	| OR_ASSIGN			{ $$ = $1; show_debug(__LINE__, $$, "assignment operator"); }
	;

expression
	: assignment_expression					{	$$ = $1; show_debug(__LINE__, $$, "expression"); }
	| expression ',' assignment_expression	{	$$ = $2; show_debug(__LINE__, $$, "expression");
												$$->child1 = $1;
												$$->child2 = $3; }
	;

constant_expression
	: conditional_expression                           {	$$ = $1; show_debug(__LINE__, $$, "constant_expression"); }
	;

declaration
	: declaration_specifiers ';'						{	$$ = $1; show_debug(__LINE__, $$, "declaration");
	 														free_tree($2); }
	| declaration_specifiers init_declarator_list ';'	{	$$ = $2; show_debug(__LINE__, $$, "declaration");
															for(tmpleaf = $2; tmpleaf != NULL; tmpleaf = tmpleaf->next) {
																if (tmpleaf->type != FUNCTION_DECLARATION) {
																	tmpleaf->type = VARIABLE_DEFINITION;
																	mix_ast_leaves(tmpleaf, $1);
																} else {
																	tmpleaf->return_value = copy_leaf($1,false);
																	tmpleaf->t_typedef = $1->t_typedef;
																	tmpleaf->return_value->pointer += $2->pointer;
																}
																if (tmpleaf->t_typedef) {
																	check_typedef(tmpleaf);
																}
															}
															free_tree($1);
															free_tree($3);
														}
	;

override_declaration
    : CRUST_OVERRIDE declaration                        { $$ = $2; $$->t_override = true; }

declaration_specifiers
	: storage_class_specifier							{	$$ = $1; show_debug(__LINE__, $$, "declaration_specifiers"); }
	| storage_class_specifier declaration_specifiers	{	$$ = $1; show_debug(__LINE__, $$, "declaration_specifiers");
															mix_ast_leaves($$, $2);
															free_tree($2); }
	| type_specifier									{	$$ = $1; show_debug(__LINE__, $$, "declaration_specifiers"); }
	| type_specifier declaration_specifiers				{	$$ = $1; show_debug(__LINE__, $$, "declaration_specifiers");
															mix_ast_leaves($$, $2);
															free_tree($2); }
	| type_qualifier									{	$$ = $1; show_debug(__LINE__, $$, "declaration_specifiers"); }
	| type_qualifier declaration_specifiers				{	$$ = $1; show_debug(__LINE__, $$, "declaration_specifiers");
															mix_ast_leaves($$, $2);
															free_tree($2); }
	| function_specifier								{	$$ = $1; show_debug(__LINE__, $$, "declaration_specifiers"); }
	| function_specifier declaration_specifiers			{	$$ = $2; show_debug(__LINE__, $$, "declaration_specifiers");
	 														// inline definition
	 														free_tree($1); }
    | T_TYPEOF '(' declaration_specifiers ')'           {   $$ = $3; show_debug(__LINE__, $$, "declaration_specifiers");
                                                            free_tree($1);
                                                            free_tree($2);
                                                            free_tree($4); }
    | T_TYPEOF '(' declaration_specifiers pointer ')'   {   $$ = $3; show_debug(__LINE__, $$, "declaration_specifiers");
                                                            $3->pointer += $4->pointer;
                                                            free_tree($1);
                                                            free_tree($2);
                                                            free_tree($4);
                                                            free_tree($5); }
	;

init_declarator_list
	: init_declarator							{	$$ = $1; show_debug(__LINE__, $$, "init_declarator_list"); }
	| init_declarator_list ',' init_declarator	{	$$ = $1; show_debug(__LINE__, $$, "init_declarator_list");
													append_next($$, $3);
													free_tree($2); }
	;

init_declarator
	: declarator					{	$$ = $1; show_debug(__LINE__, $$, "init_declarator"); }
	| declarator '=' initializer	{	$$ = $1; show_debug(__LINE__, $$, "init_declarator");
										$$->assignment = $3;
										free_tree($2); }
	;

storage_class_specifier
	: TYPEDEF		{	$$ = $1; show_debug(__LINE__, $$, "storage_class_specifier"); }
	| EXTERN		{	$$ = $1; show_debug(__LINE__, $$, "storage_class_specifier"); }
	| STATIC		{	$$ = $1; show_debug(__LINE__, $$, "storage_class_specifier"); }
	| AUTO			{	$$ = $1; show_debug(__LINE__, $$, "storage_class_specifier"); }
	| REGISTER		{	$$ = $1; show_debug(__LINE__, $$, "storage_class_specifier"); }
	;

type_specifier
	: VOID	            					{	$$ = $1; $$->type = TYPE_SPECIFIER; show_debug(__LINE__, $$, "type_specifier"); }
	| CHAR				             		{	$$ = $1; $$->type = TYPE_SPECIFIER; show_debug(__LINE__, $$, "type_specifier"); }
	| SHORT				            		{	$$ = $1; $$->type = TYPE_SPECIFIER; show_debug(__LINE__, $$, "type_specifier"); }
	| INT			               			{	$$ = $1; $$->type = TYPE_SPECIFIER; show_debug(__LINE__, $$, "type_specifier"); }
	| LONG			              			{	$$ = $1; $$->type = TYPE_SPECIFIER; show_debug(__LINE__, $$, "type_specifier"); }
	| FLOAT				               		{	$$ = $1; $$->type = TYPE_SPECIFIER; show_debug(__LINE__, $$, "type_specifier"); }
	| DOUBLE		             			{	$$ = $1; $$->type = TYPE_SPECIFIER; show_debug(__LINE__, $$, "type_specifier"); }
	| SIGNED		             			{	$$ = $1; $$->type = TYPE_SPECIFIER; show_debug(__LINE__, $$, "type_specifier"); }
	| UNSIGNED		               			{	$$ = $1; $$->type = TYPE_SPECIFIER; show_debug(__LINE__, $$, "type_specifier"); }
	| BOOL		               				{	$$ = $1; $$->type = TYPE_SPECIFIER; show_debug(__LINE__, $$, "type_specifier"); }
	| COMPLEX			               		{	$$ = $1; $$->type = TYPE_SPECIFIER; show_debug(__LINE__, $$, "type_specifier"); }
	| IMAGINARY		                  		{	$$ = $1; $$->type = TYPE_SPECIFIER; show_debug(__LINE__, $$, "type_specifier"); }
	| VA_LIST					            {	$$ = $1; $$->type = TYPE_SPECIFIER; show_debug(__LINE__, $$, "type_specifier"); }
	| struct_or_union_specifier             {	$$ = $1; $$->type = TYPE_SPECIFIER; show_debug(__LINE__, $$, "type_specifier"); }
	| enum_specifier                        {	$$ = $1; $$->type = TYPE_SPECIFIER; show_debug(__LINE__, $$, "type_specifier"); }
	| TYPE_NAME                             {	$$ = $1; $$->type = TYPE_SPECIFIER; show_debug(__LINE__, $$, "type_specifier"); }
    | T_TYPEOF '(' postfix_expression ')'   {   $$ = $1; $$->type = TYPE_SPECIFIER; $$->t_char = true;
                                                show_debug(__LINE__, $$, "type_specifier");
                                                free_tree($2);
                                                free_tree($3);
                                                free_tree($4); }
    | T_TYPEOF '(' pointer postfix_expression ')'   {   $$ = $1; $$->type = TYPE_SPECIFIER; $$->t_char = true; $$->pointer += $3->pointer;
                                                show_debug(__LINE__, $$, "type_specifier");
                                                free_tree($2);
                                                free_tree($3);
                                                free_tree($4);
                                                free_tree($5); }
	;

struct_or_union_specifier
	: struct_or_union IDENTIFIER '{' struct_declaration_list '}'	{	$$ = $1; show_debug(__LINE__, $$, "struct_or_union_specifier");
																		free($$->name);
																		$$->name = mystrdup($2->data);
																		$$->struct_data = $4;
																		free_tree($2);
																		free_tree($3);
																		free_tree($5); }
    | struct_or_union IDENTIFIER '{' '}'	                        {	$$ = $1; show_debug(__LINE__, $$, "struct_or_union_specifier");
																		free($$->name);
																		$$->name = mystrdup($2->data);
																		free_tree($2);
																		free_tree($3);
																		free_tree($4); }
	| struct_or_union '{' struct_declaration_list '}'				{	$$ = $1; show_debug(__LINE__, $$, "struct_or_union_specifier");
																		$$->struct_data = $3;
																		free_tree($2);
																		free_tree($4); }
    | struct_or_union '{' '}'	                                    {	$$ = $1; show_debug(__LINE__, $$, "struct_or_union_specifier");
																		free_tree($2);
																		free_tree($3); }
	| struct_or_union IDENTIFIER									{	$$ = $1; show_debug(__LINE__, $$, "struct_or_union_specifier");
																		free($$->name);
																		$$->name = mystrdup($2->data);
																		free_tree($2); }
    | struct_or_union TYPE_NAME                                     {	$$ = $1; show_debug(__LINE__, $$, "struct_or_union_specifier");
																		free($$->name);
																		$$->name = mystrdup($2->data);
																		free_tree($2); }
	;

struct_or_union
	: STRUCT	{	$$ = $1; show_debug(__LINE__, $$, "struct_or_union");
					$$->t_struct = true; }
	| UNION		{	$$ = $1; show_debug(__LINE__, $$, "struct_or_union");
					$$->t_union = true; }
	;

struct_declaration_list
	: struct_declaration							{	$$ = $1; show_debug(__LINE__, $$, "struct_declaration_list"); }
	| struct_declaration_list struct_declaration	{	$$ = $1; show_debug(__LINE__, $$, "struct_declaration_list");
														append_next($1, $2); }
	;

struct_declaration
	: specifier_qualifier_list ';'							{	$$ = $1; show_debug(__LINE__, $$, "struct_declaration");
																free_tree($2); }
	| specifier_qualifier_list struct_declarator_list ';'	{	$$ = $2; show_debug(__LINE__, $$, "struct_declaration");
                                                                for(tmpleaf = $2; tmpleaf != NULL; tmpleaf = tmpleaf->next) {
                                                                    mix_ast_leaves($$, $1);
                                                                }
																free_tree($1);
																free_tree($3); }
	;

specifier_qualifier_list
	: type_specifier specifier_qualifier_list	{	$$ = $1; show_debug(__LINE__, $$, "specifier_qualifier_list");
													mix_ast_leaves($$, $2);
													free_tree($2); }
	| type_specifier							{	$$ = $1; show_debug(__LINE__, $$, "specifier_qualifier_list"); }
	| type_qualifier specifier_qualifier_list	{	$$ = $1; show_debug(__LINE__, $$, "specifier_qualifier_list");
													mix_ast_leaves($$, $2);
													free_tree($2); }
	| type_qualifier							{	$$ = $1; show_debug(__LINE__, $$, "specifier_qualifier_list"); }
	;

struct_declarator_list
	: struct_declarator								{	$$ = $1; show_debug(__LINE__, $$, "struct_declarator_list"); }
	| struct_declarator_list ',' struct_declarator	{	$$ = $1; show_debug(__LINE__, $$, "struct_declarator_list");
                                                        append_next($$, $3);
                                                        free_tree($2); }
	;

struct_declarator
	: declarator							{	$$ = $1; show_debug(__LINE__, $$, "struct_declarator"); }
	| ':' constant_expression				{	$$ = new_leaf(EMPTY_DECLARATOR, $2);
												$$->bitfield = $2; show_debug(__LINE__, $$, "struct_declarator");
												free_tree($1); }
	| declarator ':' constant_expression	{	$$ = $1; show_debug(__LINE__, $$, "struct_declarator");
												$$->bitfield = $3;
												free_tree($2); }
	;

enum_specifier
	: ENUM '{' enumerator_list '}'					{	$$ = $1; show_debug(__LINE__, $$, "enum_specifier");
														$$->enum_data = $3;
														free_tree($2);
														free_tree($4); }
	| ENUM IDENTIFIER '{' enumerator_list '}'		{	$$ = $1; show_debug(__LINE__, $$, "enum_specifier");
														free($$->name);
														$$->name = mystrdup($2->data);
														$$->enum_data = $4;
														free_tree($2);
														free_tree($3);
														free_tree($5); }
	| ENUM '{' enumerator_list ',' '}'				{	$$ = $1; show_debug(__LINE__, $$, "enum_specifier");
														$$->enum_data = $3;
														free_tree($2);
														free_tree($4);
														free_tree($5); }
	| ENUM IDENTIFIER '{' enumerator_list ',' '}'	{	$$ = $1; show_debug(__LINE__, $$, "enum_specifier");
														free($$->name);
														$$->name = mystrdup($2->data);
														$$->enum_data = $4;
														free_tree($2);
														free_tree($3);
														free_tree($5);
														free_tree($6); }
	| ENUM IDENTIFIER								{	$$ = $1; show_debug(__LINE__, $$, "enum_specifier");
														free($$->name);
														$$->name = mystrdup($2->data);
														free_tree($2); }
	;

enumerator_list
	: enumerator						{	$$ = $1; show_debug(__LINE__, $$, "enumerator_list"); }
	| enumerator_list ',' enumerator	{	$$ = $1; show_debug(__LINE__, $$, "enumerator_list");
											append_next($$, $3);
											free_tree($2); }
	;

enumerator
	: IDENTIFIER							{	$$ = $1; show_debug(__LINE__, $$, "enumerator");
												free($$->name);
												$$->name = mystrdup($1->data); }
	| IDENTIFIER '=' constant_expression	{	$$ = $1; show_debug(__LINE__, $$, "enumerator");
												free($$->name);
												$$->name = mystrdup($1->data);
												$$->assignment = $3;
												free_tree($2); }
	;

type_qualifier
	: CONST				{ $$ = $1; show_debug(__LINE__, $$, "type_qualifier"); }
	| RESTRICT			{ $$ = $1; show_debug(__LINE__, $$, "type_qualifier"); }
	| VOLATILE			{ $$ = $1; show_debug(__LINE__, $$, "type_qualifier"); }
	| CRUST_T			{ $$ = $1; show_debug(__LINE__, $$, "type_qualifier"); }
	| CRUST_BORROW		{ $$ = $1; show_debug(__LINE__, $$, "type_qualifier"); }
	| CRUST_RECYCLE		{ $$ = $1; show_debug(__LINE__, $$, "type_qualifier"); }
	| CRUST_ALIAS		{ $$ = $1; show_debug(__LINE__, $$, "type_qualifier"); }
	| CRUST_NOT_NULL	{ $$ = $1; show_debug(__LINE__, $$, "type_qualifier"); }
	;

function_specifier
	: INLINE				{ $$ = $1; show_debug(__LINE__, $$, "function_specifier"); }
	;

declarator
	: pointer direct_declarator	{	$$ = $2; show_debug(__LINE__, $$, "declarator");
									$$->pointer += $1->pointer;
									free_tree($1); }
	| direct_declarator			{	$$ = $1; show_debug(__LINE__, $$, "declarator"); }
	;


direct_declarator
	: IDENTIFIER																	{	$$ = $1;
																						show_debug(__LINE__, $$, "direct_declarator");
																						free($$->name);
																						$$->name = mystrdup($1->data); }
	| '(' declarator ')'															{	$$ = $2; // definition of a pointer to function
																						show_debug(__LINE__, $$, "direct_declarator");
																						if ($$->pointer == 1) {
																							$$->function = true;
																							$$->pointer = 0;
																						}
																						free_tree($1);
																						free_tree($3); }
	| direct_declarator '[' type_qualifier_list assignment_expression ']'			{	$$ = $1; show_error(__LINE__, $$); }
	| direct_declarator '[' type_qualifier_list ']'									{	$$ = $1; show_error(__LINE__, $$); }
	| direct_declarator '[' assignment_expression ']'								{	$$ = $1;
																						show_debug(__LINE__, $$, "direct_declarator");
																						if ($$->arrays == NULL) {
																							$$->arrays = $3;
																						} else {
																							append_right($$->arrays, $3);
																						}
																						free_tree($2);
																						free_tree($4);
																					}
	| direct_declarator '[' STATIC type_qualifier_list assignment_expression ']'	{	$$ = $1; show_error(__LINE__, $$); }
	| direct_declarator '[' type_qualifier_list STATIC assignment_expression ']'	{	$$ = $1; show_error(__LINE__, $$); }
	| direct_declarator '[' type_qualifier_list '*' ']'								{	$$ = $1; show_error(__LINE__, $$); }
	| direct_declarator '[' '*' ']'													{	$$ = $1; show_error(__LINE__, $$); }
	| direct_declarator '[' ']'														{	$$ = $1;
																						show_debug(__LINE__, $$, "direct_declarator");
																						tmpleaf = new_leaf(BRACKETS_CONTAINER, $1);
																						append_right($$,tmpleaf);
																						$$->arrays = $2;
																						free_tree($3); }
	| direct_declarator '(' parameter_type_list ')'									{	$$ = $1; // List of parameters in the definition of a function
																						show_debug(__LINE__, $$, "direct_declarator");
																						$$->function_params = $3;
																						if (!$$->function) {
																							$$->type = FUNCTION_DECLARATION;
																						}
																						if ($3->t_ellipsis) {
																							$$->t_ellipsis = true;
																							$3->t_ellipsis = false;
																						}
																						free_tree($2);
																						free_tree($4); }
	| direct_declarator '(' identifier_list ')'										{	$$ = $1; show_error(__LINE__, $$); }
	| direct_declarator '(' ')'														{	$$ = $1; // Definition of a function without parameters
																						show_debug(__LINE__, $$, "direct_declarator");
																						$$->function_params = new_leaf(EMPTY_DECLARATOR, $2);
																						if (!$$->function) {
																							$$->type = FUNCTION_DECLARATION;
																						}
																						free_tree($2);
																						free_tree($3); }
	;

pointer
	: '*'												{	$$ = $1; show_debug(__LINE__, $$, "pointer");
															$$->pointer = 1; }
	| '*' type_qualifier_list							{	$$ = $2; show_debug(__LINE__, $$, "pointer");
															$$->pointer++;
															free_tree($1); }
	| '*' pointer										{	$$ = $2; show_debug(__LINE__, $$, "pointer");
															$$->pointer++;
															free_tree($1); }
	| '*' type_qualifier_list pointer					{	$$ = $2; show_debug(__LINE__, $$, "pointer");
															mix_ast_leaves($$, $3);
															$$->pointer++;
															free_tree($1);
															free_tree($3); }
	;

type_qualifier_list
	: type_qualifier									{	$$ = $1; show_debug(__LINE__, $$, "type_qualifier_list"); }
	| type_qualifier_list type_qualifier				{	$$ = $1; show_debug(__LINE__, $$, "type_qualifier_list");
															mix_ast_leaves($$, $2);
															free_tree($2); }
	;


parameter_type_list
	: parameter_list									{	$$ = $1; show_debug(__LINE__, $$, "parameter_type_list"); }
	| parameter_list ',' ELLIPSIS						{	$$ = $1; show_debug(__LINE__, $$, "parameter_type_list");
															$$->t_ellipsis = true;
															free_tree($2);
															free_tree($3); }
	;

parameter_list
	: parameter_declaration								{	$$ = $1; show_debug(__LINE__, $$, "parameter_list"); }
	| parameter_list ',' parameter_declaration			{	$$ = $1; show_debug(__LINE__, $$, "parameter_list");
															append_next($1, $3);
															free_tree($2); }
	;

parameter_declaration
	: declaration_specifiers declarator					{	$$ = $1; show_debug(__LINE__, $$, "parameter_declaration");
															mix_ast_leaves($$, $2);
															free($$->name);
															$$->name = mystrdup($2->name);
															free_tree($2); }
	| declaration_specifiers abstract_declarator		{	$$ = $1; show_debug(__LINE__, $$, "parameter_declaration");
															if (($2->type == '*') || ($2->type == CONST) || ($2->type == RESTRICT)) {
																mix_ast_leaves($$, $2);
																free_tree($2);
															} else {
                                                                if ($2->type == OWN_POINTER) {
                                                                    $$->pointer+=$2->pointer;
                                                                    free_tree($2);
                                                                } else {
																	show_error(__LINE__, $$);
                                                                }
															}
														}
	| declaration_specifiers							{	$$ = $1; show_debug(__LINE__, $$, "parameter_declaration"); }
	;

identifier_list
	: IDENTIFIER										{	$$ = $1; show_error(__LINE__, $$); }
	| identifier_list ',' IDENTIFIER					{	$$ = $1; show_error(__LINE__, $$); }
	;

type_name
	: specifier_qualifier_list							{	$$ = $1; show_debug(__LINE__, $$, "type_name"); }
	| specifier_qualifier_list abstract_declarator		{	$$ = $1; show_debug(__LINE__, $$, "type_name");
															mix_ast_leaves($$, $2);
															free_tree($2); }
	;

abstract_declarator
	: pointer											{	$$ = $1; show_debug(__LINE__, $$, "abstract_declarator"); }
	| direct_abstract_declarator						{	$$ = $1; show_debug(__LINE__, $$, "abstract_declarator"); }
	| pointer direct_abstract_declarator				{	$$ = $1; show_error(__LINE__, $$); }
	;

direct_abstract_declarator
	: '(' abstract_declarator ')'								{	$$ = $2; show_debug(__LINE__, $$, "direct_abstract_declarator");
	 																free_tree($1);
																	free_tree($3); }
	| '[' ']'													{	$$ = new_leaf(OWN_POINTER, NULL);
                                                                    show_debug(__LINE__, $$, "direct_abstract_declarator");
																	$$->pointer = 1;
                                                                    free_tree($1);
                                                                    free_tree($2); }
	| '[' assignment_expression ']'								{	$$ = new_leaf(OWN_POINTER, NULL);
                                                                    show_debug(__LINE__, $$, "direct_abstract_declarator");
																	$$->pointer = 1;
                                                                    free_tree($1);
                                                                    free_tree($2);
                                                                    free_tree($3); }
	| direct_abstract_declarator '[' ']'						{	$$ = $1; show_error(__LINE__, $$); }
	| direct_abstract_declarator '[' assignment_expression ']'	{	$$ = $1; show_error(__LINE__, $$); }
	| '[' '*' ']'												{	$$ = $1; show_error(__LINE__, $$); }
	| direct_abstract_declarator '[' '*' ']'					{	$$ = $1; show_error(__LINE__, $$); }
	| '(' ')'													{	$$ = $1; show_error(__LINE__, $$); }
	| '(' parameter_type_list ')'								{	$$ = $1; show_error(__LINE__, $$); }
	| direct_abstract_declarator '(' ')'						{	$$ = $1; show_error(__LINE__, $$); }
	| direct_abstract_declarator '(' parameter_type_list ')'	{	$$ = $1; show_debug(__LINE__, $$, "direct_abstract_declarator");
	 																free_tree($2);
																	free_tree($3);
																	free_tree($4); }
	;

initializer
	: assignment_expression								{	$$ = $1; show_debug(__LINE__, $$, "initializer"); }
	| '{' initializer_list '}'							{	$$ = $2; show_debug(__LINE__, $$, "initializer");
															free_tree($1);
															free_tree($3); }
	| '{' initializer_list ',' '}'						{	$$ = $2; show_debug(__LINE__, $$, "initializer");
	 														free_tree($1);
															free_tree($3);
															free_tree($4); }
	;

initializer_list
	: initializer										{	$$ = $1; show_debug(__LINE__, $$, "initializer_list"); }
	| designation initializer							{	$$ = $1; show_debug(__LINE__, $$, "initializer_list");
															// initializer for designated initializer in struct
	 														$$->child2 = $2; }
	| initializer_list ',' initializer					{	$$ = $1; show_debug(__LINE__, $$, "initializer_list");
															append_right($$, $3);
															free_tree($2); }
	| initializer_list ',' designation initializer		{	$$ = $1; show_debug(__LINE__, $$, "initializer_list");
	 														append_right($$, $3);
															$3->child2 = $4;
															free_tree($2); }
	;

designation
	: designator_list '=' 								{	$$ = $2; /* assignment of designated initializer in struct */
															show_debug(__LINE__, $$, "designation");
	 														$$->child1 = $1; }
	;

designator_list
	: designator										{	$$ = $1;show_debug(__LINE__, $$, "designator_list"); }
	| designator_list designator						{	$$ = $1; show_debug(__LINE__, $$, "designator_list");
															append_right($$, $2); }
	;

designator
	: '[' constant_expression ']'						{	$$ = $1; show_error(__LINE__, $$); }
	| '.' IDENTIFIER									{	$$ = $1; /* For designated initializers in structs */
                                                            $$->right = $2;
	 														show_debug(__LINE__, $$, "designator");}
	;

statement
	: labeled_statement									{	$$ = $1; show_debug(__LINE__, $$, "statement"); }
	| compound_statement								{	$$ = $1; show_debug(__LINE__, $$, "statement"); }
	| '(' statement ')'									{	$$ = $2; show_debug(__LINE__, $$, "statement");
															free_tree($1);
															free_tree($3); }
	| expression_statement								{	$$ = $1; show_debug(__LINE__, $$, "statement"); }
	| selection_statement								{	$$ = $1; show_debug(__LINE__, $$, "statement"); }
	| iteration_statement								{	$$ = $1; show_debug(__LINE__, $$, "statement"); }
	| jump_statement									{	$$ = $1; show_debug(__LINE__, $$, "statement"); }
	;

labeled_statement
	: IDENTIFIER ':' statement							{	$$ = $1; show_debug(__LINE__, $$, "labeled_statement");
															free($$->name);
	 														$$->name = mystrdup($1->data);
															$$->type = LABEL;
															append_next($$, $3);
															free_tree($2); }
	| CASE constant_expression ':' statement			{	$$ = $1; show_debug(__LINE__, $$, "labeled_statement");
	 														$$->condition = $2;
															append_next($$, $4);
															free_tree($3); }
    | CASE constant_expression ELLIPSIS constant_expression ':' statement
    			                                         {	$$ = $1; show_debug(__LINE__, $$, "labeled_statement");
                                                            int c;
                                                            bool first = true;
                                                            for(c = $2->intval; c <= $4->intval; c++) {
                                                                if (first) {
                                                                    tmpleaf = $1;
                                                                    first = false;
                                                                } else {
                                                                    tmpleaf = copy_leaf($1, false);
                                                                }
                                                                tmpleaf->condition = copy_leaf($2, false);
                                                                tmpleaf->condition->intval = c;
                                                                if (tmpleaf != $1) {
                                                                    append_next($$,tmpleaf);
                                                                }
                                                            }
															append_next($$, $6);
                                                            free_tree($2);
                                                            free_tree($3);
                                                            free_tree($4);
                                                            free_tree($5); }

	| DEFAULT ':' statement								{	$$ = $1; show_debug(__LINE__, $$, "labeled_statement");
															append_next($$, $3);
															free_tree($2); }
	;

compound_statement
	: '{' '}'											{	$$ = new_leaf(START_BLOCK, $1);
															append_next_leaf($$,END_BLOCK, $2->line);
															show_debug(__LINE__, $$, "compound_statement");
	 														free_tree($1);
															free_tree($2); }
	| '{' block_item_list '}'							{	$$ = new_leaf(START_BLOCK, $1);
															append_next($$, $2);
															append_next_leaf($$,END_BLOCK, $3->line);
															show_debug(__LINE__, $$, "compound_statement");
															free_tree($1);
															free_tree($3); }
	;

block_item_list
	: block_item										{	$$ = $1; show_debug(__LINE__, $$, "block_item_list"); }
	| block_item_list block_item						{	$$ = $1; show_debug(__LINE__, $$, "block_item_list");
															append_next($$, $2); }
	;

block_item
	: declaration										{	$$ = $1; show_debug(__LINE__, $$, "block_item"); }
	| statement											{	$$ = $1; show_debug(__LINE__, $$, "block_item"); }
	| CRUST_ENABLE										{	$$ = $1; show_debug(__LINE__, $$, "block_item"); }
	| CRUST_DISABLE										{	$$ = $1; show_debug(__LINE__, $$, "block_item"); }
	| CRUST_FULL_ENABLE									{	$$ = $1; show_debug(__LINE__, $$, "block_item"); }
	| CRUST_DEBUG										{	$$ = $1; show_debug(__LINE__, $$, "block_item"); }
	| CRUST_NO_WARNING                                  {   $$ = $1; show_debug(__LINE__, $$, "block_item"); }
    | CRUST_SET_NULL '(' IDENTIFIER ')'					{	$$ = $1; show_debug(__LINE__, $$, "block_item");
                                                            $$->child1 = $3;
                                                            free($3->name);
                                                            $3->name = strdup($3->data);
                                                            free_tree($2);
                                                            free_tree($4); }
    | CRUST_SET_NOT_NULL '(' IDENTIFIER ')'				{	$$ = $1; show_debug(__LINE__, $$, "block_item");
                                                            $$->child1 = $3;
                                                            free($3->name);
                                                            $3->name = strdup($3->data);
                                                            free_tree($2);
                                                            free_tree($4); }
	;

expression_statement
	: ';'												{	$$ = new_leaf(EMPTY_DECLARATOR, $1);
															free_tree($1);
															show_debug(__LINE__, $$, "expression_statement"); }
	| expression ';'									{	$$ = $1;
															show_debug(__LINE__, $$, "expression_statement");
	 														free_tree($2);}
	;

selection_statement
	: IF '(' expression ')' statement					{	$$ = $1; show_debug(__LINE__, $$, "selection_statement");
															$$->condition = $3;
															append_next_with_blocks($$, $5);
															free_tree($2);
															free_tree($4);
														}
	| IF '(' expression ')' statement ELSE statement	{	$$ = $1; show_debug(__LINE__, $$, "selection_statement");
															$$->condition = $3;
															append_next_with_blocks($$, $5);
															append_next($$, $6);
															append_next_with_blocks($$, $7);
															free_tree($2);
															free_tree($4);
 														}
	| SWITCH '(' expression ')' statement				{	$$ = $1; show_debug(__LINE__, $$, "selection_statement");
											 				$$->condition = $3;
															append_next_with_blocks($$, $5);
															free_tree($2);
															free_tree($4); }
	;

for_statement
	: FOR																			{	$$ = $1;
	 																					show_debug(__LINE__, $$, "for_statement");}
	| CRUST_NO_ZERO FOR																{	$$ = $2;
	 																					show_debug(__LINE__, $$, "for_statement");
	 																					$$->t_crust_no_zero = true;
																						free_tree($1); }
	| FOR CRUST_NO_ZERO																{	$$ = $1;
	 																					show_debug(__LINE__, $$, "for_statement");
																						$$->t_crust_no_zero = true;
																						free_tree($2); }
	;

while_statement
	: WHILE																			{	$$ = $1;
	 																					show_debug(__LINE__, $$, "while_statement"); }
	| CRUST_NO_ZERO WHILE															{	$$ = $2;
	 																					show_debug(__LINE__, $$, "while_statement");
	 																					$$->t_crust_no_zero = true;
																						free_tree($1); }
	| WHILE CRUST_NO_ZERO															{	$$ = $1;
	 																					show_debug(__LINE__, $$, "while_statement");
	 																					$$->t_crust_no_zero = true;
																						free_tree($2); }
	;

iteration_statement
	: while_statement '(' expression ')' statement									{	$$ = $1;
	 																					show_debug(__LINE__, $$, "iteration_statement");
	 																					$$->condition = $3;
																						append_next_with_blocks($$, $5);
																						free_tree($2);
																						free_tree($4); }
	| DO statement WHILE '(' expression ')' ';'										{	$$ = $1;
	 																					show_debug(__LINE__, $$, "iteration_statement");
																						$$->condition = $5;
																						append_next_with_blocks($$, $2);
																						free_tree($3);
																						free_tree($4);
																						free_tree($6);
																						free_tree($7); }
	| for_statement '(' expression_statement expression_statement ')' statement		{	$$ = $1;
	 																					show_debug(__LINE__, $$, "iteration_statement");
																						$$->for_ch1 = $3;
																						$$->for_ch2 = $4;
																						append_next_with_blocks($$, $6);
																						free_tree($2);
																						free_tree($5); }
	| for_statement '(' expression_statement expression_statement expression ')' statement	{	$$ = $1;
	 																					show_debug(__LINE__, $$, "iteration_statement");
																						$$->for_ch1 = $3;
																						$$->for_ch2 = $4;
																						$$->for_ch3 = $5;
																						append_next_with_blocks($$, $7);
																						free_tree($2);
																						free_tree($6); }
	| for_statement '(' declaration expression_statement ')' statement				{	$$ = $1;
	 																					show_debug(__LINE__, $$, "iteration_statement");
																						$$->for_ch1 = $3;
																						$$->for_ch2 = $4;
																						append_next_with_blocks($$, $6);
																						free_tree($2);
																						free_tree($5); }
	| for_statement '(' declaration expression_statement expression ')' statement	{	$$ = $1;
	 																					show_debug(__LINE__, $$, "iteration_statement");
																						$$->for_ch1 = $3;
																						$$->for_ch2 = $4;
																						$$->for_ch3 = $5;
																						append_next_with_blocks($$, $7);
																						free_tree($2);
																						free_tree($6); }
	;

jump_statement
	: GOTO IDENTIFIER ';'	{	$$ = $1;
	 							show_debug(__LINE__, $$, "jump_statement");
								$$->name = mystrdup($2->data);
								free_tree($2);
								free_tree($3); }
	| CONTINUE ';'			{	$$ = $1;
	 							show_debug(__LINE__, $$, "jump_statement");
	 							free_tree($2);}
	| BREAK ';'				{	$$ = $1;
	 							show_debug(__LINE__, $$, "jump_statement");
	 							free_tree($2);}
	| RETURN ';'			{	$$ = $1;
	 							show_debug(__LINE__, $$, "jump_statement");
	 							free_tree($2);}
	| RETURN expression ';'	{	$$ = $1;
	 							show_debug(__LINE__, $$, "jump_statement");
								$$->child1 = $2;
								free_tree($3); }
	;

translation_unit
	: external_declaration					{	$$ = $1;
	 											show_debug(__LINE__, $$, "translation_unit"); }
	| translation_unit external_declaration	{	$$ = $1;
	 											show_debug(__LINE__, $$, "translation_unit");
												append_next($$, $2); }
	;

external_declaration
	: function_definition	{	$$ = $1; show_debug(__LINE__, $$, "external_declaration"); }
	| declaration			{	$$ = $1; show_debug(__LINE__, $$, "external_declaration"); }
    | override_declaration	{	$$ = $1; show_debug(__LINE__, $$, "external_declaration"); }
	;

function_definition
	: declaration_specifiers declarator declaration_list compound_statement	{	$$ = $1; show_error(__LINE__, $$); }
	| declaration_specifiers declarator compound_statement					{	$$ = $2;
																				show_debug(__LINE__, $$, "function_definition");
																				$$->type = FUNCTION_DEFINITION;
																				$$->return_value = $1;
																				$$->return_value->pointer += $2->pointer;
																				$$->child1 = $3; }
	;

declaration_list
	: declaration					{	$$ = $1; show_error(__LINE__, $$); }
	| declaration_list declaration	{	$$ = $1; show_error(__LINE__, $$); }
	;


%%
#include <stdio.h>

extern char yytext[];
extern int column;

void print_lines() {
	if (previous_data_line != NULL) {
		printf(previous_data_line);
	}
	printf(current_data_line);
}

void yyerror(char const *s) {
	fflush(stdout);
	printf("\n\n");
	print_lines();
	printf("\n%*s\n%*s\n", column, "^\n", column, s);
	printf("Line: %d, file: %s\n\n",current_line, current_file);
}
