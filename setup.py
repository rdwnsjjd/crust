#! /usr/bin/env python3


# CRUST is a C Preprocessor that allows to implement in C
# some of the memory checks available in RUST
#
# CRUST is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#
# CRUST is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with RemoteH.  If not, see <http://www.gnu.org/licenses/>.

import os
import sys
import re
import subprocess
from glob import glob
from setuptools import setup, find_packages
from setuptools.command.develop import develop
from setuptools.command.install import install
sys.path.insert(0, 'src')
from crust import crust

OPTIONS = {}

def get_data_files():

	data_files = []

	data_files.append(('crust',[os.path.join('src', 'lexyacclib', 'crust_code.h'), os.path.join('src', 'lexyacclib', 'crust.so'), 'crust.h']))

	try:
		for lang_name in [f for f in os.listdir('locale')]:
			mofile = os.path.join('locale', lang_name,'LC_MESSAGES','remoteh.mo')
			target = os.path.join('.','share', 'locale', lang_name, 'LC_MESSAGES') # share/locale/fr/LC_MESSAGES/
			data_files.append((target, [mofile]))
	except:
		pass
	return data_files


params = ["make", "-C", "src/lexyacclib"]
rv = subprocess.run(params)
if (rv.returncode != 0):
	try:
		print (rv.stderr.decode("utf-8"))
	except:
		pass
	sys.exit(-1)

#here = os.path.abspath(os.path.dirname(__file__))

params_setup = {}

params_setup['name'] = 'crust'
params_setup['version'] = crust.crust.version
params_setup['description']='A C preprocessor that implements some of the memory checks available in RUST'
params_setup['long_description']="CRUST is a C Preprocessor that implements some of the memory checks available in RUST, allowing to create more secure code without the overhead of libraries or other programming techniques like reference counters or garbage collectors."
params_setup['license']='GPLv3'
params_setup['packages']=['crust']
params_setup['package_dir'] = {"crust" : "src/crust"}
params_setup['url'] = 'http://www.rastersoft.com/programas/crust.html'
params_setup['author'] = 'Sergio Costas-Rodriguez (Raster Software Vigo)'
params_setup['author_email'] = 'rastersoft@gmail.com'
params_setup['data_files'] = get_data_files()
params_setup['entry_points'] = {'console_scripts': ['crust = crust.crust:main']}

setup(**params_setup)
